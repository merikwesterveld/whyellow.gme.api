﻿using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Whyellow.GME.API.Data.Excel;
using Whyellow.GME.API.Data.PostgreSQL;
using Whyellow.GME.API.Models;
using Xunit;
using ILogger = Whyellow.GME.API.IRepositories.ILogger;

namespace Whyellow.GME.API.IntegrationTestApplication
{
    /// <summary>
    /// Test class to test integration between the API server, client, database and excel data source.
    /// </summary>
    public class ApiTest
    {
        private readonly TestServer _server;
        private readonly HttpClient _client;

        private readonly ExcelProfileRepo _excelProfileRepo;
        private readonly PostgreSQLProfileRepo _postgresqlProfileRepo;

        private DatabaseOptions options = new DatabaseOptions();

        /// <summary>
        /// Constructor to arrange everything.
        /// </summary>
        public ApiTest()
        {
            options.Configure("Host=bj9hnxfvkxfbwkt-postgresql.services.clever-cloud.com;Port=5432;Database=bj9hnxfvkxfbwkt;Username=uvuifnnyjdifabkvlf3s;Password=tlCXFNFYBMa3FlNTSYOQ;Maximum Pool Size=4");

            // Arrange
            _server = new TestServer(new WebHostBuilder()
                .UseStartup<Startup>());
            _client = _server.CreateClient();

            _excelProfileRepo = new ExcelProfileRepo(new MemoryCache(new MemoryCacheOptions()), new DbLogger(options));
            _postgresqlProfileRepo = new PostgreSQLProfileRepo(new DbLogger(options), new MemoryCache(new MemoryCacheOptions()), options);
        }

        /// <summary>
        /// Check method to see if the api client and server work good.
        /// </summary>
        [Fact]
        public async Task CheckDefaultControllerTest()
        {
            // Act
            var response = await _client.GetAsync("/api/default");
            response.EnsureSuccessStatusCode();

            string responeString = await response.Content.ReadAsStringAsync();

            // Assert
            Assert.Equal("\"integration test\"".Trim().ToLowerInvariant(),
                responeString.Trim().ToLowerInvariant());
        }
    }
}
