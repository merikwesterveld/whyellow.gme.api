FROM alpine
FROM microsoft/aspnetcore-build:2.0.0 AS build
EXPOSE 8080
WORKDIR /app
COPY . .
RUN dotnet restore Whyellow.GME.API.sln
RUN dotnet publish Whyellow.GME.API.sln --output /output --configuration Release
FROM microsoft/aspnetcore:2.0.0
COPY --from=build /output /app
COPY CasesMap.xlsx /app/CasesMap.xlsx
COPY Whyellow.GME.API.xml /app/Whyellow.GME.API.xml
WORKDIR /app
ENTRYPOINT ["dotnet", "Whyellow.GME.API.dll"]