﻿using Whyellow.GME.API.Generic_Repo;
using Whyellow.GME.API.Models;

namespace Whyellow.GME.API.IRepositories
{
    /// <summary>
    /// Interface for the attribute repo.
    /// </summary>
    public interface IAttributeTypeRepo : IRepository<AttributeType>
    {
    }
}
