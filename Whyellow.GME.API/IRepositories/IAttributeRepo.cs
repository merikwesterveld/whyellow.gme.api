﻿using Whyellow.GME.API.Generic_Repo;
using Whyellow.GME.API.Models;

namespace Whyellow.GME.API.IRepositories
{
    /// <summary>
    /// Interface for the attribute repo.
    /// </summary>
    public interface IAttributeRepo : IRepository<Attribute>
    {
        /// <summary>
        /// Method to get an attribute by name.
        /// </summary>
        /// <param name="name">The name of the attribute that needs to be found.</param>
        /// <returns>The found attribute in the datasource.</returns>
        Attribute GetByName(string name);
    }
}
