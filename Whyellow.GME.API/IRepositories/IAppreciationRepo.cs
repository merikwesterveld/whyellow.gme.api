﻿using System.Collections.Generic;
using Whyellow.GME.API.Generic_Repo;
using Whyellow.GME.API.Models;

namespace Whyellow.GME.API.IRepositories
{
    /// <summary>
    /// Interface repository for the appreciation repo.
    /// </summary>
    public interface IAppreciationRepo : IRepository<Appreciation>
    {
        /// <summary>
        /// Method to get all the appreciations from a profile.
        /// </summary>
        /// <param name="id">The ID of the profile/</param>
        /// <returns>A list of appreciations that belongs to the profile.</returns>
        List<Appreciation> GetAllByProfileId(int id);
    }
}
