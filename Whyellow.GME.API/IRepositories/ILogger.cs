﻿namespace Whyellow.GME.API.IRepositories
{
    /// <summary>
    /// Interface 
    /// </summary>
    public interface ILogger
    {
        /// <summary>
        /// Method to insert a log into the database.
        /// </summary>
        /// <param name="logLevel">The log level of the log / log event.</param>
        /// <param name="message">The supporting message of the log.</param>
        void InsertLog(string logLevel, string message);
    }
}
