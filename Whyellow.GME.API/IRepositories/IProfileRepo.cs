﻿using System.Collections.Generic;
using Whyellow.GME.API.Generic_Repo;
using Whyellow.GME.API.Models;

namespace Whyellow.GME.API.IRepositories
{
    /// <summary>
    /// Interface for the profile repo.
    /// </summary>
    public interface IProfileRepo : IRepository<Profile>
    {
        /// <summary>
        /// Method to get all the liked cases for a profile.
        /// </summary>
        /// <param name="id">The ID of the profile.</param>
        /// <returns>The list of all cases that are liked by a profile.</returns>
        List<Case> GetLikedCases(int id);

        Profile GetByAuthId(string id);
    }
}
