﻿using Whyellow.GME.API.Generic_Repo;
using Whyellow.GME.API.Models;

namespace Whyellow.GME.API.IRepositories
{
    /// <summary>
    /// Interface for the case repo.
    /// </summary>
    public interface ICaseRepo : IRepository<Case>
    {
    }
}
