﻿using System.IO;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;

namespace Whyellow.GME.API.Datasources
{
    /// <summary>
    /// Abstract class the handles the basics for all the different excel repositories.
    /// </summary>
    public abstract class ExcelRepo
    {
        /// <summary>
        /// The name of the excel file. 
        /// </summary>
        private readonly string _fileName = @"CasesMap.xlsx";

        /// <summary>
        /// The name of the model sheet.
        /// </summary>
        private readonly string _modelSheet = "model";

        /// <summary>
        /// THe name of the data sheet.
        /// </summary>
        private readonly string _dataSheet = "data";

        /// <summary>
        /// The name of the profile sheet.
        /// </summary>
        private readonly string _profileSheet = "profile";

        /// <summary>
        /// The name of the appreciation sheet.
        /// </summary>
        private readonly string _appreciationSheet = "appreciation";

        /// <summary>
        /// The name of the profile like sheet.
        /// </summary>
        private readonly string _profileLikesSheet = "profile_likes";

        /// <summary>
        /// The excel workbook.
        /// </summary>
        // ReSharper disable once InconsistentNaming
        private XSSFWorkbook Xssfwb;

        /// <summary>
        /// Method to get the model sheet from the workbook.
        /// </summary>
        /// <returns>The model sheet.</returns>
        public ISheet GetModelSheet()
        {
            return Xssfwb.GetSheet(_modelSheet);
        }

        /// <summary>
        /// Method to get the data sheet from the workbook.
        /// </summary>
        /// <returns>The data sheet.</returns>
        public ISheet GetDataSheet()
        {
            return Xssfwb.GetSheet(_dataSheet);
        }

        /// <summary>
        /// Method to get the profile sheet from the workbook.
        /// </summary>
        /// <returns>The profile sheet.</returns>
        public ISheet GetProfileSheet()
        {
            return Xssfwb.GetSheet(_profileSheet);
        }

        /// <summary>
        /// Method to get the appreciation sheet from the workbook.
        /// </summary>
        /// <returns>The appreciation sheet.</returns>
        public ISheet GetAppreciationSheet()
        {
            return Xssfwb.GetSheet(_appreciationSheet);
        }

        /// <summary>
        /// Method to get the profile like sheet from the workbook.
        /// </summary>
        /// <returns></returns>
        public ISheet GetProfileLikeSheet()
        {
            return Xssfwb.GetSheet(_profileLikesSheet);
        }

        /// <summary>
        /// Method to write the values to the excel file.
        /// </summary>
        public void Write()
        {
            using (FileStream file = new FileStream(_fileName, FileMode.Create, FileAccess.Write, FileShare.ReadWrite))
            {
                Xssfwb.Write(file);
            }
        }

        /// <summary>
        /// Method to close the filestream of the excel file.
        /// </summary>
        public void Close()
        {
            using (FileStream file = new FileStream(_fileName, FileMode.Open, FileAccess.ReadWrite))
            {
                file.Close();
            }
        }

        /// <summary>
        /// Method to open the filestream of the excel file.
        /// </summary>
        public void Open()
        {
            using (FileStream file = new FileStream(_fileName, FileMode.Open, FileAccess.Read))
            {
                Xssfwb = new XSSFWorkbook(file);
            }
        }
    }
}
