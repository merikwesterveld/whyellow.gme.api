﻿using System;
using System.Collections.Generic;
using System.Data;
using Npgsql;
using Whyellow.GME.API.Interfaces;
using Whyellow.GME.API.Models;

namespace Whyellow.GME.API.Datasources
{
    /// <summary>
    /// Abstract class that handles all the basics for the different implementations of the postgresql repositories.
    /// </summary>
    // ReSharper disable once InconsistentNaming
    public abstract class PostgreSQLRepo
    {
        /// <summary>
        /// Enum that describes the execute type.
        /// </summary>
        protected enum ExecuteType
        {
            /// <summary>
            /// Query to search an object in the database.
            /// </summary>
            Query,
            /// <summary>
            /// NonQuery for updating, inserting and removing in the database.
            /// </summary>
            NonQuery,
        };


        private readonly IDatabaseOptions _databaseOptions;

        /// <summary>
        /// The constructor of the postgresql repository implementations to make connection to the database.
        /// </summary>
        public PostgreSQLRepo(IDatabaseOptions databaseOptions)
        {
            _databaseOptions = databaseOptions;
        }

        /// <summary>
        /// Method to execute a given query.
        /// </summary>
        /// <param name="query">The query that needs to get executed by the database.</param>
        /// <param name="executeType">The type of execution (either query or non query).</param>
        /// <param name="parameters">The parameters to fill the parameterized query.</param>
        /// <returns>A data reader with the results from the database.</returns>
        protected DataTable Execute(string query, ExecuteType executeType, List<NpgsqlParameter> parameters = null)
        {
            using (NpgsqlConnection connection = new NpgsqlConnection(_databaseOptions.getConnectionString()))
            {
                connection.Open();
                switch (executeType)
                {
                    case ExecuteType.Query:
                        using (NpgsqlCommand command = new NpgsqlCommand(query, connection))
                        {
                            if (parameters != null)
                            {
                                foreach (NpgsqlParameter parameter in parameters)
                                {
                                    command.Parameters.Add(parameter);
                                }
                            }
                            using (NpgsqlDataReader reader = command.ExecuteReader())
                            {
                                DataTable dt = new DataTable();
                                dt.Load(reader);
                                return dt;
                            }
                        }
                    case ExecuteType.NonQuery:
                        using (NpgsqlCommand command = new NpgsqlCommand(query, connection))
                        {
                            if (parameters != null)
                            {
                                command.Parameters.AddRange(parameters.ToArray());
                            }
                            command.ExecuteNonQuery();
                            return null;
                        }
                    default:
                        using (NpgsqlCommand command = new NpgsqlCommand(query, connection))
                        {
                            using (NpgsqlDataReader reader = command.ExecuteReader())
                            {
                                DataTable dt = new DataTable();
                                dt.Load(reader);
                                return dt;
                            }
                        }
                }
            }
        }

        /// <summary>
        /// Method to execute scalar and get the last inserted value back.
        /// </summary>
        /// <param name="query">The query that needs to get executed through the database.</param>
        /// <param name="parameters">The parameters to fill the parameterized query.</param>
        /// <returns>THe last ID.</returns>
        protected long ExecuteScalar(string query, List<NpgsqlParameter> parameters = null)
        {
            using (NpgsqlConnection connection = new NpgsqlConnection(_databaseOptions.getConnectionString()))
            {
                connection.Open();
                using (NpgsqlCommand command = new NpgsqlCommand(query, connection))
                {
                    if (parameters != null)
                    {
                        foreach (NpgsqlParameter parameter in parameters)
                        {
                            command.Parameters.Add(parameter);
                        }
                    }
                    long result = (long)command.ExecuteScalar();
                    connection.Close();
                    return result;
                }
            }
        }

        /// <summary>
        /// Method to close the connection with the database.
        /// </summary>
        protected void CloseConnection()
        {
        }
    }
}
