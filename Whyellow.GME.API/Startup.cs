﻿using System;
using System.IO;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using Npgsql;
using Swashbuckle.AspNetCore.Swagger;
using Whyellow.GME.API.Data.Excel;
using Whyellow.GME.API.Data.PostgreSQL;
using Whyellow.GME.API.Datasources;
using Whyellow.GME.API.Interfaces;
using Whyellow.GME.API.IRepositories;
using Whyellow.GME.API.Models;

namespace Whyellow.GME.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            services.AddCors();

            services.AddTransient<IProfileRepo, PostgreSQLProfileRepo>();
            services.AddTransient<ICaseRepo, PostgreSQLCaseRepo>();
            services.AddTransient<IAttributeRepo, PostgreSQLAttributeRepo>();
            services.AddTransient<IAttributeTypeRepo, PostgreSQLAttributeTypeRepo>();
            services.AddTransient<IAppreciationRepo, PostgreSQLAppreciationRepo>();

            services.AddSingleton<IDatabaseOptions, DatabaseOptions>();

            //services.AddTransient<IProfileRepo, ExcelProfileRepo>();
            //services.AddTransient<ICaseRepo, ExcelCaseRepo>();
            //services.AddTransient<IAttributeRepo, ExcelAttributeRepo>();
            //services.AddTransient<IAttributeTypeRepo, ExcelAttributeTypeRepo>();
            //services.AddTransient<IAppreciationRepo, ExcelAppreciationRepo>();

            services.AddSingleton<ILogger, DbLogger>();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "Whyellow GME API", Version = "v1"});
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                Console.WriteLine($"PATH XML BASEPATH IS: {basePath}");
                var xmlPath = Path.Combine(basePath, "Whyellow.GME.API.xml");
                Console.WriteLine($"PATH XML IS: {xmlPath}");
                c.IncludeXmlComments(xmlPath);
            });

            services.AddSingleton<IMemoryCache, MemoryCache>();
            services.AddSession();

            string domain = $"https://{Configuration["Auth0:Domain"]}/";
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultSignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;

            }).AddCookie().AddJwtBearer(options =>
            {
                options.Authority = domain;
                options.Audience = Configuration["Auth0:ApiIdentifier"];
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IDatabaseOptions connectionString)
        {
            app.UseCors(
                options => options.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader().AllowCredentials()
            );

            NpgsqlConnection.MapEnumGlobally<Appreciation.TypeAppreciation>();

            connectionString.Configure(Configuration.GetConnectionString("PostgresConnectionString"));

            app.UseSwagger();

            app.UseAuthentication();

            app.UseStaticFiles();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Whyellow GME API V1");
            });

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSession();

            app.UseCors("Policy");

            app.UseMvc();
        }
    }
}
