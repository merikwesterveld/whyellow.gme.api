﻿
namespace Whyellow.GME.API.Models
{
    /// <summary>
    /// Abstract class so that every database entity contains at least the ID.
    /// </summary>
    public abstract class Entity
    {
        /// <summary>
        /// ID of the entity in the database.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Constructor (which can only be used by derived classes) that sets the ID of the database entity.
        /// </summary>
        /// <param name="id">The ID in the database of the entity.</param>
        protected Entity(int id)
        {
            Id = id;
        }
    }
}
