﻿using System;
using System.Collections.Generic;
using System.Linq;
using Whyellow.GME.API.IRepositories;

namespace Whyellow.GME.API.Models
{
    /// <summary>
    /// Class that holds all the information of a case.
    /// </summary>
    public class Case : Entity
    {
        /// <summary>
        /// The name of the case.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The attributes that belong to the case with the input.
        /// </summary>
        public Dictionary<string, string> Attributes { get; set; }

        /// <inheritdoc />
        /// <summary>
        /// The constructor of the case.
        /// </summary>
        /// <param name="id">The ID of the case that belongs to the database.</param>
        /// <param name="name">The name of the case.</param>
        public Case(int id, string name)
            : base(id)
        {
            Name = name;
            Attributes = new Dictionary<string, string>();
        }

        /// <summary>
        /// Method to add an attribute with input to the case.
        /// </summary>
        /// <param name="attributeName">The attribute that needs to be added to the case.</param>
        /// <param name="input">The input that needs to be given to the attribute for this case.</param>
        public void AddAttribute(string attributeName, string input)
        {
            Attributes.Add(attributeName, input);
        }

        /// <summary>
        /// Method to remove an attribute from the case.
        /// </summary>
        /// <param name="attributeName">The attribute that needs to get removed.</param>
        public void RemoveAttribute(string attributeName)
        {
            Attributes.Remove(attributeName);
        }

        /// <summary>
        /// Method to check the profile input for all the attributes.
        /// </summary>
        /// <param name="value">The profile that needs to be checked.</param>
        /// <param name="attributes">Attributes used from the cache.</param>
        /// <returns>True if all the input is correct, false if it is incorrect.</returns>
        public Boolean CheckCaseInput(Case value, List<Attribute> attributes)
        {
            foreach (KeyValuePair<string, string> kvp in value.Attributes)
            {
                Attribute attribute = attributes.FirstOrDefault(a => a.Name.Equals(kvp.Key));
                if (attribute == null)
                {
                    return false;
                }
                if (!attribute.CheckInput(kvp.Value))
                {
                    return false;
                }
            }
            return true;
        }
    }
}
