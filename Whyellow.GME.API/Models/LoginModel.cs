﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace Whyellow.GME.API.Models
{
    /// <summary>
    /// Class to create model to login and register with.
    /// </summary>
    public class LoginModel
    {
        /// <summary>
        /// The email adress of the profile that wants to login or get registered.
        /// </summary>
        [JsonRequired]
        [EmailAddress]
        public string email{ get; set; }

        /// <summary>
        /// The pssword of the profile that wants to login or get registered.
        /// </summary>
        [JsonRequired]
        [DataType(DataType.Password)]
        public string password { get; set; }

        /// <summary>
        /// Client ID of the application at the Auth0 Dashboard.
        /// </summary>
        public string client_id { get; set; } = "K9lN4brozslLrUEs0EeNIkLrgiCgV2WU";

        /// <summary>
        /// Connection of the application at the Auth0 dashboard.
        /// </summary>
        public string connection { get; set; } = "Username-Password-Authentication";

        /// <summary>
        /// The Meta data of the userd.
        /// </summary>
        public Dictionary<string, string> user_metadata { get; set; } = new Dictionary<string, string>();

        /// <summary>
        /// Constructor to create a login model.
        /// </summary>
        /// <param name="email">The email of the profile.</param>
        /// <param name="password">The password of the profile.</param>
        public LoginModel(string email, string password)
        {
            this.email = email;
            this.password = password;
        }
    }
}
