﻿
namespace Whyellow.GME.API.Models
{
    /// <summary>
    /// The class which determines for each attribute to which type it belongs and what it can and can't do.
    /// </summary>
    public class AttributeType : Entity
    {
        /// <summary>
        /// The abbreviation (which matches to that of the match function) of the attribute type.
        /// </summary>
        public string Abbreviation { get; set; }

        /// <summary>
        /// Boolean which is true if the attribute can hold more than 1 value and false if it can't.
        /// </summary>
        public bool HasMultipleValues { get; set; }

        /// <summary>
        /// Boolean which is true if the attribute can only contain numbers and false if it can also contain letters.
        /// </summary>
        public bool IsNumericValue { get; set; }

        /// <inheritdoc />
        /// <summary>
        /// Constructor for the attribute type class.
        /// </summary>
        /// <param name="id">The ID of the attribute type in the database.</param>
        /// <param name="abbreviation">The abbreviation of the attribute type which should match one of the match functions names.</param>
        /// <param name="hasMultipleValues">True if the attribute can have more than 1 value, false if it can't.</param>
        /// <param name="isNumericValue">True if the attribute can only hold number values, false if it can also contain letters.</param>
        public AttributeType(int id, string abbreviation, bool hasMultipleValues, bool isNumericValue)
            : base(id)
        {
            Abbreviation = abbreviation;
            HasMultipleValues = hasMultipleValues;
            IsNumericValue = isNumericValue;
        }
    }
}
