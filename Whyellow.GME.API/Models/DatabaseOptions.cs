﻿using System;
using Whyellow.GME.API.Interfaces;

namespace Whyellow.GME.API.Models
{
    public class DatabaseOptions : IDatabaseOptions
    {
        public string ConnectionString { get; set; }

        public DatabaseOptions()
        {

        }

        public void Configure(string connectionString)
        {
            ConnectionString = connectionString;
            if (String.IsNullOrEmpty(ConnectionString))
            {
                ConnectionString = Environment.GetEnvironmentVariable("CONNECTIONSTRING");
            }
        }

        public string getConnectionString()
        {
            return ConnectionString;
        }
    }
}
