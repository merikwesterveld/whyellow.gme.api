﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Microsoft.Extensions.Caching.Memory;
using Whyellow.GME.API.IRepositories;

namespace Whyellow.GME.API.Models
{
    /// <summary>
    /// Class that makes a separation between the controllers and the models and repositories.
    /// </summary>
    public class Logic
    {
        /// <summary>
        /// Method to get all the matches sorted.
        /// </summary>
        /// <returns>All the matches between the profile and all the cases in a sorte manner.</returns>
        public List<Match> GetSortedMatches(IProfileRepo profileRepo, IAttributeRepo attributeRepo, ICaseRepo caseRepo, Profile profile, List<Attribute> attributes, IMemoryCache memoryCache)
        {
            List<Match> matches = new List<Match>();
            List<Case> likedCases = profileRepo.GetLikedCases(profile.Id);
            List<Case> allCases = new List<Case>();

            if (!memoryCache.TryGetValue("Cases", out allCases))
            {
                allCases = caseRepo.GetAll();
                memoryCache.Set("Cases", allCases);
            }

            foreach (Case matchCase in allCases)
            {
                if (likedCases.FirstOrDefault(a => a.Id.Equals(matchCase.Id)) == null)
                {
                    matches.Add(new Match(matchCase, attributeRepo, memoryCache));
                }
            }

            foreach (Match match in matches)
            {
                match.GetTotalScore(profile);
            }
     
            matches.Sort((m1, m2) => m2.MatchScore.CompareTo(m1.MatchScore));

            return matches;
        }

        /// <summary>
        /// The logic that gives a like to a case value by a profile.
        /// </summary>
        /// <param name="caseRepo">The used case repository.</param>
        /// <param name="attributeRepo">The used attribute repository.</param>
        /// <param name="appreciationRepo">the used appreciation repository.</param>
        /// <param name="profile">The profile that gives a like.</param>
        /// <param name="caseId">The ID of the case</param>
        /// <param name="attributes">The attributes used from the cache.</param>
        /// <param name="cases">The cases used from the cache.</param>
        public Appreciation Like(ICaseRepo caseRepo, IAttributeRepo attributeRepo, IAppreciationRepo appreciationRepo, Profile profile, int caseId, List<Attribute> attributes, List<Case> cases)
        {
            List<string> values = GetValues(caseRepo, attributeRepo, profile.Id, caseId, attributes, cases);

            Appreciation appreciation = null;

            foreach (string value in values)
            {
                appreciation = profile.AddLike(caseId, value, 1, profile.Id, appreciationRepo);
            }
            return appreciation;
        }

        /// <summary>
        /// The logic that gives a dislike to a case value by a profile.
        /// </summary>
        /// <param name="caseRepo">The used case repository.</param>
        /// <param name="attributeRepo">The used attribute repository.</param>
        /// <param name="appreciationRepo">the used appreciation repository.</param>
        /// <param name="profile">The profile that gives a like.</param>
        /// <param name="caseId">The ID of the case</param>
        /// <param name="attributes">The attributes used from the cache.</param>
        /// <param name="cases">The cases used from the cache.</param>
        public Appreciation Dislike(ICaseRepo caseRepo, IAttributeRepo attributeRepo, IAppreciationRepo appreciationRepo, Profile profile, int caseId, List<Attribute> attributes, List<Case> cases)
        {
            List<String> values = GetValues(caseRepo, attributeRepo, profile.Id, caseId, attributes, cases);

            foreach (string value in values)
            {
               return profile.Adddislike(caseId, value, 1, profile.Id, appreciationRepo);
            }
            return null;
        }

        /// <summary>
        /// Method to get all the vlaues from a case.
        /// </summary>
        /// <param name="caseRepo">The used case repository.</param>
        /// <param name="attributeRepo">The used attribute repository.</param>
        /// <param name="profileId">The ID of the profile.</param>
        /// <param name="caseId">The ID of the case that receives a like or dislike.</param>
        /// <param name="attributesCache">The attributes from the cache.</param>
        /// <param name="cases">The cases used from the cache.</param>
        /// <returns></returns>
        public List<string> GetValues(ICaseRepo caseRepo, IAttributeRepo attributeRepo, int profileId, int caseId, List<Attribute> attributesCache, List<Case> cases)
        {
            Case matchCase = cases.FirstOrDefault(c => c.Id.Equals(caseId));
            List<Attribute> attributes = attributesCache.FindAll(a => a.Type.Abbreviation.Equals("T"));
            List<string> values = new List<string>();

            foreach (Attribute attribute in attributes)
            {
                string value = matchCase.Attributes[attribute.Name].ToLowerInvariant().Trim();
                if (!values.Contains(value))
                {
                    values.AddRange(matchCase.Attributes[attribute.Name].Split(",").Select(s => s.Trim().ToLowerInvariant()));
                }
            }

            return values;
        }
    }
}
