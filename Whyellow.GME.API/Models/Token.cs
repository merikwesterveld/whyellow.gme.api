﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Whyellow.GME.API.Models
{
    public class Token
    {
        public string AccessToken { get; set; }
        
        public Profile Profile { get; set; }

        public Token(string accessToken, Profile profile)
        {
            AccessToken = accessToken;
            Profile = profile;
        }
    }
}
