﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;
using Whyellow.GME.API.IRepositories;

namespace Whyellow.GME.API.Models
{
    /// <summary>
    /// The match class which supports between a case and the user's profile.
    /// </summary>
    public class Match 
    {
        /// <summary>
        /// The match score from that case and the profile.
        /// </summary>
        public double MatchScore { get; set; }

        /// <summary>
        /// The case that should be matched with the profile. 
        /// </summary>
        public Case MatchCase { get; set; }

        /// <summary>
        /// Class to calculate the total match score for all the attributes between the profile and the case.
        /// </summary>
        [JsonIgnore]
        private readonly MatchFunction _matchFunction;

        /// <summary>
        /// Attribute repository that is being used by the match class.
        /// </summary>
        [JsonIgnore]
        private readonly IAttributeRepo _repo;

        /// <summary>
        /// Used memory cache in the application.
        /// </summary>
        private readonly IMemoryCache _memoryCache;

        /// <summary>
        /// Constructor for the match class.
        /// </summary>
        /// <param name="matchCase">The case that should be matched.</param>
        /// <param name="repo">The attribute repository that is being used.</param>
        /// <param name="memoryCache">The memory cache object being used.</param>
        public Match(Case matchCase, IAttributeRepo repo, IMemoryCache memoryCache)
        {
            _matchFunction = new MatchFunction();
            MatchCase = matchCase;
            _repo = repo;
            _memoryCache = memoryCache;

            List<Attribute> attributes = new List<Attribute>();
            if (!memoryCache.TryGetValue("Attributes", out attributes))
            {
                memoryCache.Set("Attributes", repo.GetAll());
            }
        }

        /// <summary>
        /// Method to get the total score of the match between the case and the profile.
        /// </summary>
        /// <param name="profile">The profile that should be matched with the case.</param>
        /// <returns>The total score between the profile and the case.</returns>
        public void GetTotalScore(Profile profile)
        {
            double weight = 1;
            MatchScore = 1;
            List<Attribute> attributes = new List<Attribute>();
            _memoryCache.TryGetValue("Attributes", out attributes);

            foreach (KeyValuePair<string, string> kvp in profile.Attributes)
            {

                if (kvp.Value.Trim().Equals(""))
                {
                    break;
                }

                Attribute attribute = attributes.FirstOrDefault(a => a.Name.Equals(kvp.Key));

                switch (attribute.Type.Abbreviation)
                {
                    case "RP":
                        string[] splitedCaseRP = attribute.Settings.Split(",").Select(s => s.Trim()).ToArray();
                        double rpScore = _matchFunction.RP(Convert.ToDouble(splitedCaseRP[0]), Convert.ToDouble(splitedCaseRP[1]),
                            Convert.ToDouble(splitedCaseRP[2]), Convert.ToDouble(kvp.Value),
                            Convert.ToDouble(MatchCase.Attributes[kvp.Key]));

                        if (!Double.IsNaN(rpScore))
                        {
                            MatchScore = MatchScore + rpScore * attribute.Weight;
                        }
                        break;
                    case "RN":
                        double rnScore = _matchFunction.RN(Convert.ToDouble(kvp.Value), Convert.ToDouble(MatchCase.Attributes[kvp.Key]));
                        if (!Double.IsNaN(rnScore))
                        {
                            MatchScore = MatchScore + rnScore * attribute.Weight;
                        }
                        break;
                    case "RF":
                        string[] splitedCaseRF =
                            attribute.Settings.Split(",").Select(s => s.Trim()).ToArray();
                        double rfScore = _matchFunction.RF(Convert.ToDouble(kvp.Value),
                            Convert.ToDouble(splitedCaseRF[0]), Convert.ToDouble(splitedCaseRF[1]));
                        if (!Double.IsNaN(rfScore))
                        {
                            MatchScore = MatchScore + rfScore * attribute.Weight;
                        }
                        break;
                    case "EM":
                        double emScore = _matchFunction.EM(kvp.Value, MatchCase.Attributes[kvp.Key]);
                        if (!Double.IsNaN(emScore))
                        {
                            MatchScore = MatchScore + emScore * attribute.Weight;
                        }
                        break;
                    case "ES":
                        double esScore = _matchFunction.ES(kvp.Value, MatchCase.Attributes[kvp.Key]);
                        if (!Double.IsNaN(esScore))
                        {
                            MatchScore = MatchScore + esScore * attribute.Weight;
                        }
                        break;
                }
                weight = weight + attribute.Weight;
            }

            foreach (Attribute a in attributes)
            {
                if (a.Type.Abbreviation.Equals("T"))
                {
                    double tScore = _matchFunction.T(profile, MatchCase.Attributes[a.Name]);
                    if (!Double.IsNaN(tScore))
                    {
                        MatchScore = MatchScore + tScore * a.Weight;
                    }
                    weight = weight + a.Weight;
                }
            }

            MatchScore = MatchScore / weight;
        }
    }
}
