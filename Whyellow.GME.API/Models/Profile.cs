﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Whyellow.GME.API.IRepositories;
using Newtonsoft.Json;

namespace Whyellow.GME.API.Models
{
    /// <summary>
    /// Class that contains all the information of the user's profile.
    /// </summary>
    public class Profile : Entity
    {
        /// <summary>
        /// All the attributes with input from the profile.
        /// </summary>
        public Dictionary<string, string> Attributes { get; set; }

        /// <summary>
        /// List of all the likes and the dislikes from the profile (for the type type attributes).
        /// </summary>
        [JsonIgnore]
        public List<Appreciation> LikesAndDislikes { get; set; } = new List<Appreciation>();

        /// <summary>
        /// The authId of the profile which was gotten from the Auth0 registration.
        /// </summary>
        [Required]
        public string authId { get; set; }

        /// <inheritdoc />
        /// <summary>
        /// Constructor to create an instance of the profile.
        /// </summary>
        /// <param name="id">the database id.</param>
        public Profile(int id)
            : base(id)
        {
            Attributes = new Dictionary<string, string>();
        }

        /// <summary>
        /// Method to add an attribute to the profile with input.
        /// </summary>
        /// <param name="attributeName">The attribute that needs to be added to the profile.</param>
        /// <param name="input">The input / value for the profile for that attribute.</param>
        public void AddAttribute(string attributeName, string input)
        {
            if (!Attributes.ContainsKey(attributeName))
            {
                Attributes.Add(attributeName, input);
            }
        }

        /// <summary>
        /// Method to remove an attribute from the profile.
        /// </summary>
        /// <param name="attributeName">The attribute that needs to be removed.</param>
        public void RemoveAttribute(string attributeName)
        {
            if (Attributes.ContainsKey(attributeName))
            {
                Attributes.Remove(attributeName);
            }
        }

        /// <summary>
        /// Method to add a like to the profile.
        /// </summary>
        /// <param name="caseId">The Case ID.</param>
        /// <param name="value">The value that needs to get a like.</param>
        /// <param name="amount">The amount of likes that need to be added.</param>
        /// <param name="profileId">The ID of the profile that likes a certain value.</param>
        /// <param name="appreciationRepo">The appreciation repository used.</param>
        public Appreciation AddLike(int caseId, string value, int amount, int profileId, IAppreciationRepo appreciationRepo)
        {
            if (LikesAndDislikes.Any(a => a.Value.ToLowerInvariant().Trim().Equals(value.ToLowerInvariant().Trim()) && a.Type.Equals(Appreciation.TypeAppreciation.Like)))
            {
                Appreciation appreciation = LikesAndDislikes.FirstOrDefault(a => a.Value.ToLowerInvariant().Trim().Equals(value.ToLowerInvariant().Trim()) && a.Type.Equals(Appreciation.TypeAppreciation.Like));
                LikesAndDislikes.Remove(appreciation);
                if (appreciation != null)
                {
                    appreciation.Add(amount);
                    LikesAndDislikes.Add(appreciation);
                    appreciation.CaseId = caseId;
                    appreciationRepo.Update(appreciation);
                }
                return appreciation;
            }
            else
            {
                Appreciation appreciation = appreciationRepo.Insert(new Appreciation(caseId, value, amount,
                    Appreciation.TypeAppreciation.Like,
                    profileId, caseId));
                LikesAndDislikes.Add(appreciation);
                return appreciation;
            }
        }

        /// <summary>
        /// Method to add a dislike to the profile.
        /// </summary>
        /// <param name="caseId">The ID of the case that needs to get a like or dislike.</param>
        /// <param name="value">The value that needs to get a dislike.</param>
        /// <param name="amount">The amount of dislikes that need to be added.</param>
        /// <param name="profileId">The ID of the profile that likes something.</param>
        /// <param name="appreciationRepo">The appreciation repo being used.</param>
        public Appreciation Adddislike(int caseId, string value, int amount, int profileId, IAppreciationRepo appreciationRepo)
        {
            if (LikesAndDislikes.Any(a =>
                a.Value.ToLowerInvariant().Trim().Equals(value.ToLowerInvariant().Trim()) && a.Type.Equals(Appreciation.TypeAppreciation.Dislike)))
            {
                Appreciation appreciation = LikesAndDislikes.FirstOrDefault(a => a.Value.ToLowerInvariant().Trim().Equals(value.ToLowerInvariant().Trim()) && a.Type.Equals(Appreciation.TypeAppreciation.Dislike));
                LikesAndDislikes.Remove(appreciation);
                if (appreciation != null)
                {
                    appreciation.Add(amount);
                    LikesAndDislikes.Add(appreciation);
                    appreciation.CaseId = caseId;
                    appreciationRepo.Update(appreciation);
                }

                return appreciation;
            }
            else
            {
                Appreciation appreciation = appreciationRepo.Insert(new Appreciation(caseId, value, amount,
                    Appreciation.TypeAppreciation.Dislike,
                    profileId, caseId));
                LikesAndDislikes.Add(appreciation);
                return appreciation;
            }
        }

        /// <summary>
        /// Method to check the profile input for all the attributes.
        /// </summary>
        /// <param name="value">The profile that needs to be checked.</param>
        /// <param name="attributes">Attributes used from the cache.</param>
        /// <returns>True if all the input is correct, false if it is incorrect.</returns>
        public bool CheckProfileInput(Profile value, List<Attribute> attributes)
        {
            foreach (KeyValuePair<string, string> kvp in value.Attributes)
            {
                Attribute attribute = attributes.FirstOrDefault(a => a.Name.Equals(kvp.Key));
                if (attribute == null)
                {
                    return false;
                }
                if (!attribute.CheckInput(kvp.Value))
                {
                    return false;
                }
            }
            return true;
        }
    }
}
