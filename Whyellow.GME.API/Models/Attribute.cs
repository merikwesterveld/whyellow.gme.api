﻿using System;
using System.Linq;

namespace Whyellow.GME.API.Models
{
    /// <summary>
    /// The class that holds all the information about the attribute.
    /// </summary>
    public class Attribute
    {
        /// <summary>
        /// The name of the attribute.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The type of attribute.
        /// </summary>
        public AttributeType Type { get; set; }

        /// <summary>
        /// The settings of the attribute (which can be empty if no settings for this kind of attribute are needed).
        /// </summary>
        public string Settings { get; set; }

        /// <summary>
        /// The weight of the attribute relative to all the other attributes.
        /// </summary>
        public double Weight { get; set; }

        /// <summary>
        /// Constructor of all the present fields for the attribute class.
        /// </summary>
        /// <param name="name">The name of the attribute.</param>
        /// <param name="type">The type of attribute.</param>
        /// <param name="settings">The settings of the attribute.</param>
        /// <param name="weight">The weight of the attribute relative to all the other attributes.</param>
        public Attribute(string name, AttributeType type, string settings, double weight)
        {
            Name = name;
            Type = type;
            Settings = settings;
            Weight = weight;
        }

        /// <inheritdoc />
        /// <summary>
        /// Constructor of the attribute clas without the settings field.
        /// </summary>
        /// <param name="name">The name of the attribute.</param>
        /// <param name="type">The type of attribute.</param>
        /// <param name="weight">The weight of the attribute relative to all the other attributes.</param>
        public Attribute(string name, AttributeType type, double weight)
        {
            Name = name;
            Type = type;
            Weight = weight;
        }

        /// <summary>
        /// Method to check if the input for an attribute is correct.
        /// </summary>
        /// <param name="input">The input of the attribute.</param>
        /// <returns>True if the input of the attribute is correct, fals if it isn't.</returns>
        public bool CheckInput(string input)
        {
            if ((!Type.HasMultipleValues && input.Contains(",")) || (Type.IsNumericValue && !input.All(char.IsDigit)))
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Method to check if the model is correct.
        /// </summary>
        /// <returns>True if the model is correct, false if it is incorrect.</returns>
        public bool CheckModel()
        {
            if ((Type.Abbreviation.Equals("RP") || Type.Abbreviation.Equals("RF")) &&(Settings == null))
            {
                return false;
            }
            if ((Type.Abbreviation.Equals("RP") || Type.Abbreviation.Equals("RF")) && (Settings.Equals(String.Empty)))
            {
                return false;
            }
            return true;
        }
    }
}
