﻿
namespace Whyellow.GME.API.Models
{
    /// <summary>
    /// Class that contains the information of a like or dislike by a profile for a case attribute with the type type.
    /// </summary>
    public class Appreciation : Entity
    {
        /// <summary>
        /// Enum which determines wheter an appreciation is a like or a dislike.
        /// </summary>
        public enum TypeAppreciation
        {
            /// <summary>
            /// Appreciation type which indicates that the appreciation is a like.
            /// </summary>
            Like = 0,
            /// <summary>
            /// Appreciation type which indicates that the appreciation is a dislike.
            /// </summary>
            Dislike = 1
        };
        
        /// <summary>
        /// The Id of the case attribute to which this appreciation belongs.
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// The amount of likes or dislikes of this appreciation.
        /// </summary>
        public double Amount { get; set; }

        /// <summary>
        /// The type of appreciation (like or dislike).
        /// </summary>
        public TypeAppreciation Type { get; set; }

        /// <summary>
        /// ProfileID that belongs to this appreciation (needed for the database).
        /// </summary>
        public int ProfileId { get; set; }

        /// <summary>
        /// The CaseID that belongs to this appreciation.
        /// </summary>
        public int CaseId { get; set; }

        /// <inheritdoc />
        /// <summary>
        /// Constructor for the appreciation class.
        /// </summary>
        /// <param name="id">The database ID of the like or the dislike.</param>
        /// <param name="value">The value of the appreciation.</param>
        /// <param name="amount">The amount of likes or dislikes of the appreciation.</param>
        /// <param name="type">The type of appreciation, either like or dislike.</param>
        /// <param name="profileId">The id of the profile that gives a like or dislike.</param>
        /// <param name="caseId">The id of the case that a profile gives a like or dislike to.</param>
        public Appreciation(int id, string value, double amount, TypeAppreciation type, int profileId, int caseId) 
            : base (id)
        {
            Value = value;
            Amount = amount;
            Type = type;
            ProfileId = profileId;
            CaseId = caseId;
        }

        /// <inheritdoc />
        /// <summary>
        /// Constructor for the appreciation class.
        /// </summary>
        /// <param name="id">The database ID of the like or the dislike.</param>
        /// <param name="value">The value of the appreciation.</param>
        /// <param name="amount">The amount of likes or dislikes of the appreciation.</param>
        /// <param name="type">The type of appreciation, either like or dislike.</param>
        /// <param name="profileId">The Id of the profile that liked or disliked.</param>
        public Appreciation(int id, string value, double amount, TypeAppreciation type, int profileId)
            : base(id)
        {
            Value = value;
            Amount = amount;
            Type = type;
            ProfileId = profileId;
        }

        /// <summary>
        /// Add a like or dislike to the case attribute.
        /// </summary>
        /// <param name="value">The amount of likes or dislikes (most of the time this is 1.).</param>
        public void Add(double value)
        {
            Amount = Amount + value;
        }

        /// <summary>
        /// Subtract a like or dislike from the case attribute.
        /// </summary>
        /// <param name="value">The amount of likes or dislikes (most of the time this is 1.).</param>
        public void Subtract(double value)
        {
            Amount = Amount = value;
        }
    }
}
