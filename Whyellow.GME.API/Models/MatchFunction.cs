﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Whyellow.GME.API.Models
{
    /// <summary>
    /// Class that holds all the functions for matching a case and a profile.
    /// </summary>
    public class MatchFunction
    {
        /// <summary>
        /// The partial range match functions.
        /// </summary>
        /// <param name="lowBound">The lower bound of the case for the partial range match function.</param>
        /// <param name="upBound">The upper bound of the case for the partial range match function.</param>
        /// <param name="partial">The partial value from the ramp.</param>
        /// <param name="profileValue">The value given by the profile.</param>
        /// <param name="caseValue">The value of the case.</param>
        /// <returns>A number between 0 and 100 where 100 means best match and 0 means worst match.</returns>
        public double RP(double lowBound, double upBound, double partial, double profileValue, double caseValue)
        {
            if (profileValue < (caseValue - lowBound - partial))
            {
                return 0;
            }
            if (caseValue - lowBound <= profileValue && profileValue <= caseValue + upBound)
            {
                return 100;
            }
            if (profileValue > (caseValue + upBound + partial))
            {
                return 0;
            }
            if ((caseValue - lowBound - partial) <= profileValue && profileValue < caseValue - lowBound)
            {
                double firstHalf = profileValue - (caseValue - lowBound - partial);
                double secondHalf = caseValue - lowBound - (caseValue - lowBound - partial);

                if (Math.Abs(firstHalf) > 0 && Math.Abs(secondHalf) > 0)
                {
                    return 100 * (firstHalf / secondHalf);
                }
                return 0;
            }
            if (caseValue + upBound < profileValue && profileValue <= (caseValue + upBound + partial))
            {
                double firstHalf = (caseValue + upBound + partial) - profileValue;
                double secondHalf = (caseValue + upBound + partial) - caseValue + upBound;

                if (Math.Abs(firstHalf) > 0 && Math.Abs(secondHalf) > 0)
                {
                    return 100 * (firstHalf / secondHalf);
                }
                return 0;
            }
            return 0;
        }

        /// <summary>
        /// The match function to match multiple elements.
        /// </summary>
        /// <param name="profileValues">The elements given by the profile for the attribute.</param>
        /// <param name="caseValues">The elemens given by the case.</param>
        /// <returns>A number between 0 and 100 where 100 means best match and 0 means worst match.</returns>
        public double EM(string profileValues, string caseValues)
        {
            List<string> profileValuesSplited = profileValues.Split(",").OfType<string>().Select(s => s.Trim().ToLowerInvariant()).ToList();
            List<string> caseValuesSplited = caseValues.Split(",").OfType<string>().Select(s => s.Trim().ToLowerInvariant()).ToList();

            double amountOfIntersections = profileValuesSplited.Intersect(caseValuesSplited).Count();
            double profileValuesSplitedCount = Convert.ToDouble(profileValuesSplited.Count);
            double caseValueSplitedCount = Convert.ToDouble(caseValuesSplited.Count);

            if (Math.Abs(amountOfIntersections) > 0 && Math.Abs(profileValuesSplitedCount) > 0 && Math.Abs(caseValueSplitedCount) > 0)
            {
                return ((amountOfIntersections / profileValuesSplitedCount) /
                        (caseValueSplitedCount - amountOfIntersections + 1)) * 100;
            }

            return 0;
        }

        /// <summary>
        /// The match function to match a Type attribute.
        /// </summary>
        /// <param name="profile">The profile to match with this type for the like and dislike profile.</param>
        /// <param name="caseValues">The value of the case to which the profile needs to match.</param>
        /// <returns>A number between 0 and 100 where 100 means best match and 0 means worst match.</returns>
        public double T(Profile profile, string caseValues)
        {
            string[] caseValuesSplited = caseValues.Split(",").Select(s => s.Trim().ToLowerInvariant()).ToArray();
            double amountLikes = 1;
            double amountLikesAndDislikes = 1;

            foreach (string s in caseValuesSplited)
            {
                Appreciation appreciationLike;
                Appreciation appreciationDislike;
                if (profile.LikesAndDislikes.FirstOrDefault(a => a.Value.Equals(s)) != null)
                {
                    appreciationLike = profile.LikesAndDislikes.FirstOrDefault(a => a.Value.Equals(s) && a.Type.Equals(Appreciation.TypeAppreciation.Like));
                    if (appreciationLike != null)
                    {
                        amountLikes = amountLikes + appreciationLike.Amount;
                    }
                    appreciationDislike = profile.LikesAndDislikes.FirstOrDefault(a => a.Value.Equals(s) && a.Type.Equals(Appreciation.TypeAppreciation.Dislike));
                    if (appreciationDislike != null)
                    {
                        if (appreciationLike != null)
                        {
                            amountLikesAndDislikes =
                                amountLikesAndDislikes + appreciationLike.Amount + appreciationDislike.Amount;
                        }
                        else
                        {
                            amountLikesAndDislikes =
                                amountLikesAndDislikes + 0 + appreciationDislike.Amount;
                        }
                    }
                    else
                    {
                        if (appreciationLike != null)
                        {
                            amountLikesAndDislikes = amountLikesAndDislikes + appreciationLike.Amount;
                        }
                    }
                }
            }

            if (Math.Abs(amountLikes) > 0 && Math.Abs(amountLikesAndDislikes) > 0)
            {
                if ((amountLikes / amountLikesAndDislikes) * 100 > 100)
                {
                    return 100;
                }

                return (amountLikes / amountLikesAndDislikes) * 100;
            }
            return 0;
        }

        /// <summary>
        /// The match function for a fit range. 
        /// </summary>
        /// <param name="profileValue">The value of the profile.</param>
        /// <param name="caseBottom">The bottom value of the case for the range.</param>
        /// <param name="caseTop">The top value of the case for the range.</param>
        /// <returns>A number between 0 and 100 where 100 means best match and 0 means worst match.</returns>
        public double RF(double profileValue, double caseBottom, double caseTop)
        {
            if (profileValue >= caseBottom && profileValue <= caseTop)
            {
                return 100;
            }
            return 0;
        }

        /// <summary>
        /// The match function for the no range.
        /// </summary>
        /// <param name="profileValue"></param>
        /// <param name="caseValue"></param>
        /// <returns></returns>
        public double RN(double profileValue, double caseValue)
        {
            if (profileValue.Equals(caseValue))
            {
                return 100;
            }
            return 0;
        }

        /// <summary>
        /// The match function for single elements. 
        /// </summary>
        /// <param name="profileValue">The value given to the profile.</param>
        /// <param name="caseValue">The value of the case.</param>
        /// <returns>A number between 0 and 100 where 100 means best match and 0 means worst match.</returns>
        public double ES(string profileValue, string caseValue)
        {
            if (profileValue.Trim().ToLowerInvariant().Equals(caseValue.Trim().ToLowerInvariant()))
            {
                return 100;
            }
            return 0;
        }
    }
}
