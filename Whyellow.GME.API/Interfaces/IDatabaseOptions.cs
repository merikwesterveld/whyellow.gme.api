﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Whyellow.GME.API.Interfaces
{
    public interface IDatabaseOptions
    {
        void Configure(string connectionString);

        string getConnectionString();
    }
}
