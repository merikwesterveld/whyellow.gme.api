﻿using System;
using System.Collections.Generic;
using System.Linq;
using NPOI.SS.UserModel;
using Whyellow.GME.API.Data.PostgreSQL;
using Whyellow.GME.API.Datasources;
using Whyellow.GME.API.IRepositories;
using Whyellow.GME.API.Models;

namespace Whyellow.GME.API.Data.Excel
{
    /// <summary>
    /// Class for the implementation of the excel appreciation repository.
    /// </summary>
    public class ExcelAppreciationRepo : ExcelRepo, IAppreciationRepo
    {
        /// <summary>
        /// Logger used for logging the application.
        /// </summary>
        private readonly ILogger _logger;

        /// <summary>
        /// Constructor to create the excel appreciation repository implementation.
        /// </summary>
        /// <param name="logger">The logger used to log the application events.</param>
        public ExcelAppreciationRepo(ILogger logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// Get all the appreciations.
        /// </summary>
        /// <returns>The list of appreciations from the datasource.</returns>
        public List<Appreciation> GetAll()
        {
            List<Appreciation> appreciations = new List<Appreciation>();
            try
            {
                Open();
                ISheet appreciationSheet = GetAppreciationSheet();

                if (appreciationSheet != null)
                {
                    for (int rowNum = 1; rowNum <= appreciationSheet.LastRowNum; rowNum++)
                    {
                        IRow row = appreciationSheet.GetRow(rowNum);

                        if (row != null && row.GetCell(0) != null && row.GetCell(1) != null && row.GetCell(2) != null &&
                         row.GetCell(3) != null)
                        {
                            Appreciation.TypeAppreciation type = Appreciation.TypeAppreciation.Like;
                            if (row.GetCell(2).ToString().ToLower().Equals("dislike"))
                            {
                                type = Appreciation.TypeAppreciation.Dislike;
                            }

                            Appreciation appreciation = new Appreciation(rowNum, row.GetCell(1).ToString(), Convert.ToDouble(row.GetCell(3).ToString()), type, Convert.ToInt32(row.GetCell(0).ToString()));
                            appreciations.Add(appreciation);
                        }
                    }
   
                    _logger.InsertLog(DbLogger.LoggingEvents.INFORMATION.ToString(), "Successfully got all the appreciations at ExcelAppreciationRepo.Get().");
                }
                else
                {
                    _logger.InsertLog(DbLogger.LoggingEvents.ERROR.ToString(), "The Appreciationsheet at ExcelAppreciationRepo.Get() isn't available.");
                }

                Close();
            }
            catch (Exception e)
            {
                _logger.InsertLog(DbLogger.LoggingEvents.ERROR.ToString(), "Couldn't get all the appreciations at ExcelAppreciationRepo.Get() because of " +
                 $"an error, stacktrace: {e.Message}.");
            }
            finally
            {
                Close();
            }
            return appreciations;
        }

        /// <summary>
        /// Method to get an appreciation by the id.
        /// </summary>
        /// <param name="id">The Id of the appreciation that needs to be found.</param>
        /// <returns>The Appreciation with that id.</returns>
        public Appreciation GetById(int id)
        {
            Appreciation appreciation = null;
            try
            {
                Open();

                ISheet appreciationSheet = GetAppreciationSheet();

                if (appreciationSheet != null)
                {
                    IRow row = appreciationSheet.GetRow(id);

                    if (row != null && row.GetCell(0) != null && row.GetCell(1) != null && row.GetCell(2) != null &&
                     row.GetCell(3) != null)
                    {
                        Appreciation.TypeAppreciation type = Appreciation.TypeAppreciation.Like;
                        if (row.GetCell(2).ToString().ToLower().Equals("dislike"))
                        {
                            type = Appreciation.TypeAppreciation.Dislike;
                        }

                        appreciation = new Appreciation(Convert.ToInt32(id), row.GetCell(1).ToString(),
                         Convert.ToDouble(row.GetCell(3).ToString()), type,
                         Convert.ToInt32(row.GetCell(0).ToString()));
                        _logger.InsertLog(DbLogger.LoggingEvents.INFORMATION.ToString(), $"Successfully got all the appreciations at ExcelAppreciationRepo.GetById() with Id {id}.");
                    }
                    _logger.InsertLog(DbLogger.LoggingEvents.WARNING.ToString(), $"Couldn't get the appreciation at ExcelApprecitation.GetById() with Id {id}. This could be because of " +
                     "an error or the appreciation with that id isn't present in the data source.");
                }
                else
                {
                    _logger.InsertLog(DbLogger.LoggingEvents.ERROR.ToString(), $"The Appreciationsheet at ExcelAppreciationRepo.GetById() with Id {id} isn't available.");
                }

                Close();
            }
            catch (Exception e)
            {
                _logger.InsertLog(DbLogger.LoggingEvents.ERROR.ToString(), "Couldn't get all the appreciations at ExcelAppreciationRepo.GetById() with Id being" +
                 $"{id} because of an error, stacktrace: {e.Message}.");
            }
            finally
            {
                Close();
            }
            return appreciation;
        }

        /// <summary>
        /// Method to insert a new appreciation into the datasource.
        /// </summary>
        /// <param name="obj">The appreciation that needs to be inserted into the datasource.</param>
        /// <returns>The inserted appreciation.</returns>
        public Appreciation Insert(Appreciation obj)
        {
            try
            {
                Open();

                ISheet appreciationSheet = GetAppreciationSheet();

                if (appreciationSheet != null)
                {

                    IRow row = appreciationSheet.CreateRow(appreciationSheet.LastRowNum + 1);

                    if (row != null)
                    {
                        ICell cell = row.CreateCell(0);
                        cell.SetCellValue(row.RowNum);

                        row.CreateCell(0).SetCellValue(obj.ProfileId);
                        row.CreateCell(1).SetCellValue(obj.Value);
                        row.CreateCell(2).SetCellValue(obj.Type.ToString());
                        row.CreateCell(3).SetCellValue(obj.Amount);

                        obj.Id = row.RowNum;

                        Write();
                    }
                    _logger.InsertLog(DbLogger.LoggingEvents.INFORMATION.ToString(), "Successfully got all the appreciations at ExcelAppreciationRepo.Insert().");
                }
                else
                {
                    _logger.InsertLog(DbLogger.LoggingEvents.ERROR.ToString(), "The Appreciationsheet at ExcelAppreciationRepo.Insert() isn't available.");
                }
                Close();
            }
            catch (Exception e)
            {
                _logger.InsertLog(DbLogger.LoggingEvents.ERROR.ToString(), "Couldn't get all the appreciations at ExcelAppreciationRepo.Insert() because of " +
                 $"an error, stacktrace: {e.Message}.");
            }
            finally
            {
                Close();
            }

            setProfileCaseLike(obj);

            return obj;
        }

        /// <summary>
        /// Method to remove the appreciation from the datasource.
        /// </summary>
        /// <param name="obj">The appreciation that needs to get removed out of the datasource.</param>
        public void Remove(Appreciation obj)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Method to set the cases liked by the profile.
        /// </summary>
        /// <param name="obj">The appreciation that the profile gave to a case.</param>
        public void setProfileCaseLike(Appreciation obj)
        {
            try
            {
                Open();
                ISheet profileLikesSheet = GetProfileLikeSheet();

                if (profileLikesSheet != null)
                {
                    IRow rowProfileLikes = profileLikesSheet.CreateRow(profileLikesSheet.LastRowNum + 1);

                    if (rowProfileLikes != null)
                    {
                        ICell cellProfileLikes = rowProfileLikes.CreateCell(0);
                        cellProfileLikes.SetCellValue(rowProfileLikes.RowNum);

                        rowProfileLikes.CreateCell(0).SetCellValue(obj.ProfileId);
                        rowProfileLikes.CreateCell(1).SetCellValue(obj.CaseId);

                        Write();
                    }
                    _logger.InsertLog(DbLogger.LoggingEvents.INFORMATION.ToString(), "Successfully got all the appreciations at ExcelAppreciationRepo.Insert().");
                }
                else
                {
                    _logger.InsertLog(DbLogger.LoggingEvents.ERROR.ToString(), "The ProfileLikeSheet at ExcelAppreciationRepo.Insert() isn't available.");
                }
                Close();
            }
            catch (Exception e)
            {
                _logger.InsertLog(DbLogger.LoggingEvents.ERROR.ToString(), "Couldn't get all the appreciations at ExcelAppreciationRepo.Insert() because of " +
                                                                           $"an error, stacktrace: {e.Message}.");
            }
            finally
            {
                Close();
            }
        }

        /// <summary>
        /// Method to update an appreciation in the datasource.
        /// </summary>
        /// <param name="obj">The appreciation that needs to get updated.</param>
        /// <returns>The updated appreciation.</returns>
        public Appreciation Update(Appreciation obj)
        {
            try
            {
                Open();
                ISheet appreciationSheet = GetAppreciationSheet();

                if (appreciationSheet != null)
                {
                    IRow row = appreciationSheet.GetRow(obj.Id);

                    if (row != null)
                    {
                        row.GetCell(1).SetCellValue(obj.Value);
                        row.GetCell(2).SetCellValue(obj.Type.ToString());
                        row.GetCell(3).SetCellValue(obj.Amount);

                        Write();

                        _logger.InsertLog(DbLogger.LoggingEvents.INFORMATION.ToString(), $"Successfully updated the given appreciation with id being {obj.Id} at ExcelAppreciationRepo.Update()");
                    }
                    _logger.InsertLog(DbLogger.LoggingEvents.WARNING.ToString(), $"Couldn't update the appreciation at ExcelAppreciationRepo.Update() with the Id being {obj.Id} because it couldn't" +
                     " be found. This could be an error or the appreciation isn't available in the datasource.");
                }
                _logger.InsertLog(DbLogger.LoggingEvents.ERROR.ToString(), "The appreciationSheet could not be found at ExcelAppreciationRepo.Update().");

                Close();
            }
            catch (Exception e)
            {
                _logger.InsertLog(DbLogger.LoggingEvents.ERROR.ToString(), $"There occurred an error at ExcelApprectiationRepo.Update(). with stacktrace {e.Message}.");
            }
            finally
            {
                Close();
            }

            setProfileCaseLike(obj);

            return obj;
        }

        /// <summary>
        /// Method to get all the appreciations for the profile by their id.
        /// </summary>
        /// <param name="id">The ID of the profile.</param>
        /// <returns>A list of appreciations from a profile.</returns>
        public List<Appreciation> GetAllByProfileId(int id)
        {
            try
            {
                Open();
                List<Appreciation> allAppreciations = GetAll();

                Close();
                _logger.InsertLog(DbLogger.LoggingEvents.INFORMATION.ToString(), $"Successfully got all the appreciations from a profile with Id being {id}");
                return allAppreciations.Where(a => a.ProfileId.Equals(id)).ToList();
            }
            catch (Exception e)
            {
                _logger.InsertLog(DbLogger.LoggingEvents.ERROR.ToString(), $"Couldn't get the appreciations for the profile with the id {id} because of an " +
                 $"error. Stacktrace: {e.StackTrace}");
            }
            finally
            {
                Close();
            }
            return null;
        }
    }
}