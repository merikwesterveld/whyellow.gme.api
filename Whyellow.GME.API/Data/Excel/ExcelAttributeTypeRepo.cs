﻿using System;
using System.Collections.Generic;
using NPOI.SS.UserModel;
using Whyellow.GME.API.Data.PostgreSQL;
using Whyellow.GME.API.Datasources;
using Whyellow.GME.API.IRepositories;
using Whyellow.GME.API.Models;

namespace Whyellow.GME.API.Data.Excel
{
    /// <summary>
    /// The implementation for the attributetype repo from the excel datasource.
    /// </summary>
    public class ExcelAttributeTypeRepo : ExcelRepo, IAttributeTypeRepo
    {
        /// <summary>
        /// Logger to log all the activities in the application.
        /// </summary>
        private readonly ILogger _logger;

        /// <summary>
        /// Constructor to create the ExcelAttributeTypeRepo implementation.
        /// </summary>
        /// <param name="logger">The logger that is being used to log all the acitivities in the application.</param>
        public ExcelAttributeTypeRepo(ILogger logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// Method to get all the attribute types from the excel repo.
        /// </summary>
        /// <returns></returns>
        public List<AttributeType> GetAll()
        {
            return new List<AttributeType>() {
                new AttributeType(1, "RP", false, true),
                new AttributeType(2, "RF", false, true),
                new AttributeType(3, "RN", false, true),
                new AttributeType(4, "ES", false, false),
                new AttributeType(5, "EM", true, false),
                new AttributeType(6, "T", true, false)
            };
        }

        /// <summary>
        /// Get the attribute type by the ID of an attribute.
        /// </summary>
        /// <param name="id">The ID of the attribute.</param>
        /// <returns>The corresponding attribute type.</returns>
        public AttributeType GetById(int id)
        {
            try
            {
                Open();
                ISheet modelSheet = GetModelSheet();

                if (modelSheet != null)
                {
                    IRow row = modelSheet.GetRow(id);

                    if (modelSheet.GetRow(id) != null)
                    {
                        switch (row.GetCell(1).ToString().ToUpper())
                        {
                            case "RP":
                                AttributeType rp = new AttributeType(id, "RP", false, true);
                                Close();
                                return rp;
                            case "RF":
                                AttributeType rf = new AttributeType(id, "RF", false, true);
                                Close();
                                return rf;
                            case "RN":
                                AttributeType rn = new AttributeType(id, "RN", false, true);
                                Close();
                                return rn;
                            case "ES":
                                AttributeType es = new AttributeType(id, "ES", false, false);
                                Close();
                                return es;
                            case "EM":
                                AttributeType em = new AttributeType(id, "EM", true, false);
                                Close();
                                return em;
                            case "T":
                                AttributeType t = new AttributeType(id, "T", true, false);
                                Close();
                                return t;
                            default:
                                Close();
                                return null;
                        }
                    }
                }
                else
                {
                    _logger.InsertLog(DbLogger.LoggingEvents.ERROR.ToString(),
                     $"Couldn't find the model sheet at ExcelAttributeTypeRepo.GetByID() where the ID is {id}.");
                }
            }
            catch (Exception e)
            {
                _logger.InsertLog(DbLogger.LoggingEvents.ERROR.ToString(), $"Couldn't find the model with Id being {id} " +
                 $"because an error occurred with stacktrace: {e.Message}.");
            }
            finally
            {
                Close();
            }

            return null;
        }

        /// <summary>
        /// Method to insert an attributetype into the datasource.
        /// </summary>
        /// <param name="obj">The attributetype that needs to get inserted into excel.</param>
        /// <returns>The inserted attributetype.</returns>
        public AttributeType Insert(AttributeType obj)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Method to remove an attributetype from the datasource.
        /// </summary>
        /// <param name="obj">The attributetype object that needs to get removed from the excel datsource.</param>
        public void Remove(AttributeType obj)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Method to update an attributetype in the excel datasource.
        /// </summary>
        /// <param name="obj">The attributetype object that needs to get updated in the excel datasource.</param>
        /// <returns>The updated attributetype.</returns>
        public AttributeType Update(AttributeType obj)
        {
            throw new NotImplementedException();
        }
    }
}