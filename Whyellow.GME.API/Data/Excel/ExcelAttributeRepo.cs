﻿using System;
using System.Collections.Generic;
using System.Linq;
using NPOI.SS.UserModel;
using Whyellow.GME.API.Data.PostgreSQL;
using Whyellow.GME.API.Datasources;
using Whyellow.GME.API.IRepositories;
using Whyellow.GME.API.Models;
using Attribute = Whyellow.GME.API.Models.Attribute;

namespace Whyellow.GME.API.Data.Excel
{
    /// <summary>
    /// Implementation of the attribute excel repository.
    /// </summary>
    public class ExcelAttributeRepo : ExcelRepo, IAttributeRepo
    {
        /// <summary>
        /// The attributetype repo that is needed for the implementation.
        /// </summary>
        private readonly ExcelAttributeTypeRepo _attributeTypeRepo;

        /// <summary>
        /// Logger used to log activities from the application.
        /// </summary>
        private readonly ILogger _logger;

        /// <summary>
        /// Constructor for the excel attribute repo.
        /// </summary>
        public ExcelAttributeRepo(ILogger logger)
        {
            _logger = logger;
            _attributeTypeRepo = new ExcelAttributeTypeRepo(logger);
        }

        /// <summary>
        /// Method to get all the attributes from the excel data source.
        /// </summary>
        /// <returns>A list of all the attributes from the excel data source.</returns>
        public List<Attribute> GetAll()
        {
            List<Attribute> attributes = new List<Attribute>();

            try
            {
                Open();
                ISheet modelSheet = GetModelSheet();

                if (modelSheet != null)
                {
                    for (int rowNum = 1; rowNum <= modelSheet.LastRowNum; rowNum++)
                    {
                        IRow row = modelSheet.GetRow(rowNum);
                        if (modelSheet.GetRow(rowNum) != null)
                        {
                            string settings = String.Empty;

                            if (row.GetCell(2) != null && row.GetCell(2).CellType != CellType.Blank)
                            {
                                settings = row.GetCell(2).ToString();
                            }

                            AttributeType attributeType = _attributeTypeRepo.GetById(rowNum);
                            if (attributeType != null)
                            {
                                double weight = 1;
                                if (Convert.ToDouble(row.GetCell(3).ToString()) > 0)
                                {
                                    weight = Convert.ToDouble(row.GetCell(3).ToString());
                                }

                                Attribute attribute = new Attribute(row.GetCell(0).ToString(), attributeType,
                                 settings, weight);
                                if (attribute.CheckModel())
                                {
                                    attributes.Add(attribute);
                                }
                                else
                                {
                                    _logger.InsertLog(DbLogger.LoggingEvents.ERROR.ToString(), "Couldn't get all the attributes because at ExcelAttributeRepo.GetAll() " +
                                     "because one of the attributes has wrong values.");
                                    Close();
                                    return null;
                                }
                            }
                            else
                            {
                                _logger.InsertLog(DbLogger.LoggingEvents.ERROR.ToString(), "Couldn't get all the attributes at ExcelAttributeRepo.GetAll() " +
                                 "because one of the attributes doesn't have a type.");
                                Close();
                                return null;
                            }
                        }
                    }
                }
                else
                {
                    _logger.InsertLog(DbLogger.LoggingEvents.ERROR.ToString(), "Couldn't get all the attributes at ExcelAttributeRepo.GetAll() because " +
                     "the model sheet could not be found.");
                }

                Close();
            }
            catch (Exception e)
            {
                _logger.InsertLog(DbLogger.LoggingEvents.ERROR.ToString(), "Couldn't get all the attributes at ExcelAttributeRepo.GetAll()" +
                 $" because an error occurred with stacktrace: {e.Message}.");
                return null;
            }
            finally
            {
                Close();
            }
            _logger.InsertLog(DbLogger.LoggingEvents.INFORMATION.ToString(), "Successfully got all the attributes at ExcelAttributeRepo.GetAll().");
            return attributes;
        }

        /// <summary>
        /// Method to get an attribute by his id.
        /// </summary>
        /// <param name="id">The id of the attribute that needs to be found.</param>
        /// <returns>The found attribute with the id.</returns>
        public Attribute GetById(int id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Method to get an attribute by id.
        /// </summary>
        /// <param name="name">The name that belongs to the attribute that you need from the datasource.</param>
        /// <returns>The attribute by the id.</returns>
        public Attribute GetByName(string name)
        {
            Attribute attribute = GetAll().FirstOrDefault(a => a.Name.Equals(name));
            if (attribute != null)
            {
                _logger.InsertLog(DbLogger.LoggingEvents.INFORMATION.ToString(), $"Successfully got the attribute with name {name} at ExcelAttributeRepo.GetByName().");
                return attribute;
            }
            _logger.InsertLog(DbLogger.LoggingEvents.WARNING.ToString(), $"Couldn't get the attribute with name {name} at ExcelAttributeRepo.GetByName() because " +
             "either an error occurred or the attribute isn't in the datasource.");
            return null;
        }

        /// <summary>
        /// Method to insert an attribute into the excel attribute repo.
        /// </summary>
        /// <param name="obj">The attribute that needs to get inserted.</param>
        /// <returns>The inserted attribute.</returns>
        public Attribute Insert(Attribute obj)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Method to remove an attribute from the excel attribute repo.
        /// </summary>
        /// <param name="obj">The attribute that needs to get removed from the excel data source.</param>
        public void Remove(Attribute obj)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Method to update an attribute from the excel data source.
        /// </summary>
        /// <param name="obj">The attribute that needs to get updated in the excel data source.</param>
        /// <returns>The updated attribute.</returns>
        public Attribute Update(Attribute obj)
        {
            throw new NotImplementedException();
        }
    }
}