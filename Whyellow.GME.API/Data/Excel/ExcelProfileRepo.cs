﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.Extensions.Caching.Memory;
using NPOI.SS.UserModel;
using Whyellow.GME.API.Data.PostgreSQL;
using Whyellow.GME.API.Datasources;
using Whyellow.GME.API.IRepositories;
using Whyellow.GME.API.Models;
using Attribute = Whyellow.GME.API.Models.Attribute;

namespace Whyellow.GME.API.Data.Excel
{
    /// <summary>
    /// Class implementation to the profile repo from the excel data source.
    /// </summary>
    public class ExcelProfileRepo : ExcelRepo, IProfileRepo
    {
        /// <summary>
        /// Excel attribute repository that is needed for the implementation.
        /// </summary>
        private readonly ExcelAttributeRepo _attributeRepo;

        /// <summary>
        /// Appreciation repository whcih is needed for the implementation of this repository.
        /// </summary>
        private readonly ExcelAppreciationRepo _appreciationRepo;

        /// <summary>
        /// Case repository which is needed for the implementation of this repository.
        /// </summary>
        private readonly ExcelCaseRepo _caseRepo;

        /// <summary>
        /// Logger used to log all the activities of the application.
        /// </summary>
        private readonly ILogger _logger;

        private readonly IMemoryCache _memoryCache;

        private readonly List<Attribute> _cacheAttributes = new List<Attribute>();

        /// <summary>
        /// Constructor to create an instance of the excel profile repository.
        /// </summary>
        public ExcelProfileRepo(IMemoryCache memoryCache, ILogger logger)
        {
            _logger = logger;
            _attributeRepo = new ExcelAttributeRepo(logger);
            _appreciationRepo = new ExcelAppreciationRepo(logger);
            _caseRepo = new ExcelCaseRepo(logger, memoryCache);
            _memoryCache = memoryCache;

            if (!_memoryCache.TryGetValue("Attributes", out _cacheAttributes) && _attributeRepo != null)
            {
                _cacheAttributes = _attributeRepo.GetAll();
                _memoryCache.Set("Attributes", _cacheAttributes);
            }
        }

        /// <summary>
        /// Method to insert a profile into the excel data source.
        /// </summary>
        /// <param name="obj">The profile that needs to get inserted.</param>
        /// <returns>The inserted profile.</returns>
        public Profile Insert(Profile obj)
        {
            List<Attribute> attributes = new List<Attribute>();

            if (!_memoryCache.TryGetValue("Attributes", out attributes) && _attributeRepo != null)
            {
                attributes = _attributeRepo.GetAll();
                _memoryCache.Set("Attributes", attributes);
            }


            try
            {
                Open();
                ISheet profileSheet = GetProfileSheet();

                if (profileSheet != null)
                {
                    IRow row = profileSheet.CreateRow(profileSheet.LastRowNum + 1);

                    if (row != null)
                    {
                        ICell cell = row.CreateCell(0);
                        cell.SetCellValue(row.RowNum);

                        int cellNum = 1;

                        if (obj.Attributes.Count > 0)
                        {
                            foreach (Attribute attribute in attributes)
                            {
                                if (!attribute.Type.Abbreviation.Equals("T"))
                                {
                                    row.CreateCell(cellNum).SetCellValue(obj.Attributes[attribute.Name]);
                                    cellNum++;
                                }
                            }

                        }

                        row.CreateCell(attributes.Count(a => !a.Type.Abbreviation.Equals("T")) + 1)
                         .SetCellValue(obj.authId);

                        Write();
                    }
                    Close();
                }
                else
                {
                    _logger.InsertLog(DbLogger.LoggingEvents.ERROR.ToString(), "Couldn't insert the profile at ExcelProfileRepo.Insert() because " +
                     " the profile sheet could not be found.");
                }
            }
            catch (Exception e)
            {
                _logger.InsertLog(DbLogger.LoggingEvents.ERROR.ToString(), $"Couldn't insert the profile at ExcelProfilerepo.Insert(); because an error occurred " +
                 $"with stacktrace: {e.Message}.");
            }
            finally
            {
                Close();
            }

            return obj;
        }


        /// <summary>
        /// Method to update a profile in the excel data source.
        /// </summary>
        /// <param name="obj">The updated profile that needs to get updated in the excel data source.</param>
        /// <returns>The updated profile.</returns>
        public Profile Update(Profile obj)
        {
            List<Attribute> attributes = new List<Attribute>();

            if (!_memoryCache.TryGetValue("Attributes", out attributes) && _attributeRepo != null)
            {
                attributes = _attributeRepo.GetAll();
                _memoryCache.Set("Attributes", attributes);
            }


            try
            {
                Open();
                ISheet profileSheet = GetProfileSheet();

                if (profileSheet != null)
                {
                    IRow row = profileSheet.GetRow(obj.Id);

                    int cellNum = 1;

                    if (row != null)
                    {
                        foreach (Attribute attribute in attributes)
                        {
                            if (!attribute.Type.Abbreviation.Equals("T"))
                            {
                                if (row.GetCell(cellNum) != null)
                                {
                                    row.GetCell(cellNum).SetCellValue(obj.Attributes[attribute.Name]);
                                }
                                else
                                {
                                    row.CreateCell(cellNum).SetCellValue(obj.Attributes[attribute.Name]);
                                }
                                cellNum++;
                            }
                        }
                    }

                    Write();
                }
                else
                {
                    _logger.InsertLog(DbLogger.LoggingEvents.ERROR.ToString(), $"Couldn't update the profile with id {obj.Id} at ExcelProfileRepo.Update() " +
                     $"because it couldn't find the profile sheet.");
                }

                Close();
            }
            catch (Exception e)
            {
                _logger.InsertLog(DbLogger.LoggingEvents.ERROR.ToString(), $"Couldn't update the profile with id {obj.Id} at ExcelProfileRepo.Update() " +
                 $"because an error occurred with stacktrace: {e.Message}.");
            }
            finally
            {
                Close();
            }

            return obj;
        }

        /// <summary>
        /// Method to get all the profiles from the excel data source.
        /// </summary>
        /// <returns>A list of profiles present in the excel data source.</returns>
        public List<Profile> GetAll()
        {
            List<Profile> profiles = new List<Profile>();
            List<Attribute> attributes = new List<Attribute>();

            if (!_memoryCache.TryGetValue("Attributes", out attributes) && _attributeRepo != null)
            {
                attributes = _attributeRepo.GetAll();
                _memoryCache.Set("Attributes", attributes);
            }

            try
            {
                Open();
                ISheet profileSheet = GetProfileSheet();

                if (profileSheet != null)
                {
                    int cellNum = 1;
                    int authIdNum = attributes.Count(a => !a.Type.Abbreviation.Equals("T")) + 1;

                    for (int rowNum = 1; rowNum <= profileSheet.LastRowNum; rowNum++)
                    {
                        IRow row = profileSheet.GetRow(rowNum);
                        if (profileSheet.GetRow(rowNum) != null)
                        {
                            Profile profile = new Profile(rowNum);
                            foreach (Attribute attribute in attributes)
                            {
                                if (!attribute.Type.Abbreviation.Equals("T") && row.GetCell(cellNum) != null)
                                {
                                    profile.AddAttribute(attribute.Name, row.GetCell(cellNum).ToString());
                                    cellNum++;
                                }
                            }

                            if (row.GetCell(authIdNum) != null)
                            {
                                profile.authId = row.GetCell(authIdNum).ToString();
                            }
                            cellNum = 1;
                            profiles.Add(profile);
                            profile.LikesAndDislikes.AddRange(_appreciationRepo.GetAllByProfileId(profile.Id));

                            if (!profile.CheckProfileInput(profile, _cacheAttributes))
                            {
                                _logger.InsertLog(DbLogger.LoggingEvents.ERROR.ToString(), "Couldn't get all the profiles at ExcelProfilerepo.GetAll() because " +
                                 "one of the profile attribute values is not correct.");
                                Close();
                                return null;
                            }
                        }
                    }
                }
                else
                {
                    _logger.InsertLog(DbLogger.LoggingEvents.ERROR.ToString(), "Couldn't get all the profiles at ExcelProfileRepo.GetAll() because" +
                     " the profile sheet could not be found.");
                }
            }
            catch (Exception e)
            {
                _logger.InsertLog(DbLogger.LoggingEvents.ERROR.ToString(), "Couldn't get all the profiles at ExcelProfileRepo.GetAll() because an error occurred with" +
                 $" stacktrace: {e.Message}.");
            }
            finally
            {
                Close();
            }

            return profiles;
        }

        /// <summary>
        /// Method to get a profile by the ID in the excel data source.
        /// </summary>
        /// <param name="id">The ID that belongs to the profile in the excel data source.</param>
        /// <returns>The found profile by the id.</returns>
        public Profile GetById(int id)
        {
            List<Attribute> attributes = new List<Attribute>();

            if (!_memoryCache.TryGetValue("Attributes", out attributes) && _attributeRepo != null)
            {
                attributes = _attributeRepo.GetAll();
                _memoryCache.Set("Attributes", attributes);
            }

            try
            {
                Open();
                ISheet profileSheet = GetProfileSheet();

                if (profileSheet != null)
                {
                    IRow row = profileSheet.GetRow(id);
                    int cellNum = 1;
                    int authIdNum = attributes.Count(a => !a.Type.Abbreviation.Equals("T")) + 1;

                    if (profileSheet.GetRow(id) != null)
                    {
                        Profile profile = new Profile(id);

                        foreach (Attribute attribute in attributes)
                        {
                            if (!attribute.Type.Abbreviation.Equals("T") && row.GetCell(cellNum) != null)
                            {
                                profile.AddAttribute(attribute.Name, row.GetCell(cellNum).ToString());
                                cellNum++;
                            }
                        }

                        if (row.GetCell(authIdNum) != null)
                        {
                            profile.authId = row.GetCell(authIdNum).ToString();
                        }

                        profile.LikesAndDislikes.AddRange(_appreciationRepo.GetAllByProfileId(profile.Id));
                        if (!profile.CheckProfileInput(profile, _cacheAttributes))
                        {
                            _logger.InsertLog(DbLogger.LoggingEvents.ERROR.ToString(),
                             $"Couldn't get the profile with Id {id} at ExcelProfileRepo.GetById() " +
                             $"because the attribute values aren't correct.");
                            Close();
                            return null;
                        }

                        Close();
                        return profile;
                    }
                    else
                    {
                        _logger.InsertLog(DbLogger.LoggingEvents.WARNING.ToString(), $"Couldn't get the profile with id {id} at ExcelProfileRepo.GetById() " +
                         "what could mean that an error occurred or the profile doesn't exist in the " +
                         "datasource.");
                    }
                }
                else
                {
                    _logger.InsertLog(DbLogger.LoggingEvents.ERROR.ToString(), $"Couldn't get the profile with id {id} at ExcelProfileRepo.GetById() " +
                     $"because the profile sheet could not be found.");
                }
                Close();
            }
            catch (Exception e)
            {
                _logger.InsertLog(DbLogger.LoggingEvents.ERROR.ToString(), $"Couldn't get the profile with id {id} at ExcelProfileRepo.GetById() " +
                 $"because an error occurred stacktrace: {e.Message}");
            }
            finally
            {
                Close();
            }
            return null;
        }

        /// <summary>
        /// Method to remove a profile from the excel data source.
        /// </summary>
        /// <param name="obj">The profile that needs to get removed.</param>
        public void Remove(Profile obj)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Method to get all the liked cases for a profile.
        /// </summary>
        /// <param name="id">The ID of the profile to get all the liked cases for.</param>
        /// <returns>A list of all the liked cases from the profile.</returns>
        public List<Case> GetLikedCases(int id)
        {
            List<Case> cases = new List<Case>();

            try
            {
                Open();
                ISheet profileLikeSheet = GetProfileLikeSheet();

                if (profileLikeSheet != null)
                {
                    for (int rowNum = 1; rowNum <= profileLikeSheet.LastRowNum; rowNum++)
                    {
                        IRow row = profileLikeSheet.GetRow(rowNum);

                        if (row != null && Convert.ToInt32(row.GetCell(0).ToString()).Equals(id))
                        {
                            cases.Add(_caseRepo.GetById(Convert.ToInt32(row.GetCell(1).ToString())));
                        }
                    }
                }
                else
                {
                    _logger.InsertLog(DbLogger.LoggingEvents.ERROR.ToString(), "Couldn't get all the liked cases at ExcelProfilerepo.GetLikedCases() " +
                     $" with id {id} because the profileLikeSheet could not be found.");
                }

                Close();
            }
            catch (Exception e)
            {
                _logger.InsertLog(DbLogger.LoggingEvents.ERROR.ToString(), "Couldn't get all the liked cases at ExcelProfileRepo.GetLikedCases() with" +
                 $" id {id} because an error occurred with stacktrace: {e.Message}");
            }
            finally
            {
                Close();
            }
            return cases;
        }

        public Profile GetByAuthId(string id)
        {
            return GetAll().FirstOrDefault(p => p.authId.Equals(id));
        }
    }
}