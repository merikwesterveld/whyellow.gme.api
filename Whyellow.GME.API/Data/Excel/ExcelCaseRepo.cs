﻿using System;
using System.Collections.Generic;
using Microsoft.Extensions.Caching.Memory;
using NPOI.SS.UserModel;
using Whyellow.GME.API.Data.PostgreSQL;
using Whyellow.GME.API.Datasources;
using Whyellow.GME.API.IRepositories;
using Whyellow.GME.API.Models;
using Attribute = Whyellow.GME.API.Models.Attribute;

namespace Whyellow.GME.API.Data.Excel
{
    /// <summary>
    /// The class implementation from the Case repository in the excel datasource.
    /// </summary>
    public class ExcelCaseRepo : ExcelRepo, ICaseRepo
    {
        /// <summary>
        /// The attribute repository that is needed for the implementation.
        /// </summary>
        private readonly ExcelAttributeRepo _attributeRepo;

        /// <summary>
        /// Logger used in the application to log all the activities.
        /// </summary>
        private readonly ILogger _logger;

        private readonly IMemoryCache _memoryCache;

        private readonly List<Attribute> _cacheAttributes = new List<Attribute>();

        /// <summary>
        /// The constructor to create an instance of the excel case repo.
        /// </summary>
        public ExcelCaseRepo(ILogger logger, IMemoryCache memoryCache)
        {
            _logger = logger;
            _attributeRepo = new ExcelAttributeRepo(logger);
            _memoryCache = memoryCache;

            if (!_memoryCache.TryGetValue("Attributes", out _cacheAttributes) && _attributeRepo != null)
            {
                _cacheAttributes = _attributeRepo.GetAll();
                _memoryCache.Set("Attributes", _cacheAttributes);
            }
        }

        /// <summary>
        /// Method to get all the cases from the excel data source.
        /// </summary>
        /// <returns>A list of all the cases from the excel data source.</returns>
        public List<Case> GetAll()
        {
            List<Case> cases = new List<Case>();
            List<Attribute> attributes = new List<Attribute>();

            if (!_memoryCache.TryGetValue("Attributes", out attributes) && _attributeRepo != null)
            {
                attributes = _attributeRepo.GetAll();
                _memoryCache.Set("Attributes", attributes);
            }

            try
            {
                Open();
                ISheet dataSheet = GetDataSheet();

                if (dataSheet != null)
                {
                    for (int rowNum = 1; rowNum <= dataSheet.LastRowNum; rowNum++)
                    {
                        IRow row = dataSheet.GetRow(rowNum);
                        if (dataSheet.GetRow(rowNum) != null)
                        {
                            Case modelCase = new Case(rowNum, row.GetCell(0).ToString());

                            int cell = 1;

                            foreach (Attribute attribute in attributes)
                            {
                                modelCase.AddAttribute(attribute.Name, row.GetCell(cell).ToString());
                                cell++;
                            }

                            if (!modelCase.CheckCaseInput(modelCase, _cacheAttributes))
                            {
                                _logger.InsertLog(DbLogger.LoggingEvents.ERROR.ToString(), "Couldn't get all the cases becuase the attribute value(s) of one of the " +
                                 "cases isn't correct.");
                                Close();
                                return null;
                            }

                            cases.Add(modelCase);
                        }
                    }
                }
                else
                {
                    _logger.InsertLog(DbLogger.LoggingEvents.ERROR.ToString(), "Couldn't find the data sheet at ExcelCaseRepo.GetAll().");
                }
                Close();
            }
            catch (Exception e)
            {
                _logger.InsertLog(DbLogger.LoggingEvents.ERROR.ToString(), "Couldn't find the cases because an error has occrurred with " +
                 $"Stacktrace: {e.Message}.");
            }
            finally
            {
                Close();
            }
            return cases;
        }

        /// <summary>
        /// Method to get case by the corresponding Id.
        /// </summary>
        /// <param name="id">The Id of the case.</param>
        /// <returns>The found case by the Id.</returns>
        public Case GetById(int id)
        {
            List<Attribute> attributes = new List<Attribute>();

            if (!_memoryCache.TryGetValue("Attributes", out attributes) && _attributeRepo != null)
            {
                attributes = _attributeRepo.GetAll();
                _memoryCache.Set("Attributes", attributes);
            }

            try
            {
                Open();
                ISheet dataSheet = GetDataSheet();

                if (dataSheet != null)
                {
                    IRow row = dataSheet.GetRow(id);
                    if (dataSheet.GetRow(id) != null)
                    {
                        Case modelCase = new Case(id, row.GetCell(0).ToString());

                        int cell = 1;

                        foreach (Attribute attribute in attributes)
                        {
                            modelCase.AddAttribute(attribute.Name, row.GetCell(cell).ToString());
                            cell++;
                        }

                        if (!modelCase.CheckCaseInput(modelCase, _cacheAttributes))
                        {
                            _logger.InsertLog(DbLogger.LoggingEvents.ERROR.ToString(),
                             $"Couldn't get the case at ExcelCaseRepo.GetById() with Id {id} because the attribute values aren\'t correct.");
                            Close();
                            return null;
                        }

                        Close();
                        return modelCase;
                    }
                }
                else
                {
                    _logger.InsertLog(DbLogger.LoggingEvents.ERROR.ToString(), $"Couldn't find the data sheet at ExcelCaseRepo.GetById() with id {id}.");
                }
                Close();
            }
            catch (Exception e)
            {
                _logger.InsertLog(DbLogger.LoggingEvents.ERROR.ToString(), $"Couldn't get the case with the id {id} at ExcelCaseRepo.GetById() because" +
                 $" an error occurred with stacktrace: {e.Message}.");
            }
            finally
            {
                Close();
            }
            return null;
        }

        /// <summary>
        /// Method to insert a case into the excel data source.
        /// </summary>
        /// <param name="obj">The case that needs to get inserted into the excel data source.</param>
        /// <returns>The inserted case.</returns>
        public Case Insert(Case obj)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Method to remove a case from the excel data source.
        /// </summary>
        /// <param name="obj">The case that needs to get removed from the excel data source.</param>
        public void Remove(Case obj)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Method to update a case in the excel data source.
        /// </summary>
        /// <param name="obj">The case that needs to get updated.</param>
        /// <returns>The updated case.</returns>
        public Case Update(Case obj)
        {
            throw new NotImplementedException();
        }
    }
}