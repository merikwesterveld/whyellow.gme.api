﻿using System;
using System.Collections.Generic;
using System.Data;
using Microsoft.Extensions.Caching.Memory;
using Npgsql;
using Whyellow.GME.API.Datasources;
using Whyellow.GME.API.Interfaces;
using Whyellow.GME.API.IRepositories;
using Whyellow.GME.API.Models;
using Attribute = Whyellow.GME.API.Models.Attribute;

namespace Whyellow.GME.API.Data.PostgreSQL
{
    /// <summary>
    /// Class implementation for the postgresql case repository.
    /// </summary>
    public class PostgreSQLCaseRepo : PostgreSQLRepo, ICaseRepo
    {
        /// <summary>
        /// Logger used to log activities in the application.
        /// </summary>
        private readonly ILogger _logger;

        /// <summary>
        /// The attribute repository of the postgresql database.
        /// </summary>
        private readonly PostgreSQLAttributeRepo _attributeRepo;

        private readonly List<Attribute> _cacheAttributes = new List<Attribute>();

        /// <summary>
        /// Constructor to create an instance of the PostgreSQLCaserepo implementation.
        /// </summary>
        /// <param name="logger">The logger used to log activities from the application.</param>
        /// <param name="memoryCache">Memory cache used for caching throughout the application</param>
        public PostgreSQLCaseRepo(ILogger logger, IMemoryCache memoryCache, IDatabaseOptions databaseOptions) : base(databaseOptions)
        {
            _attributeRepo = new PostgreSQLAttributeRepo(logger, databaseOptions);
            _logger = logger;

            if (!memoryCache.TryGetValue("Attributes", out _cacheAttributes) && _attributeRepo != null)
            {
                _cacheAttributes = _attributeRepo.GetAll();
                memoryCache.Set("Attributes", _cacheAttributes);
            }
        }

        /// <summary>
        /// Method to get all the cases that are present inside the postgresql database.
        /// </summary>
        /// <returns>A list with all the cases from the database.</returns>
        public List<Case> GetAll()
        {
            List<Case> cases = new List<Case>();

            int caseId = -1;
            string name = String.Empty;

            try
            {
                using (DataTable table = Execute("SELECT m.*, a.*, t.*, ca.input FROM Modelcase m inner join case_attribute ca on m.id = ca.caseid " +
                 "inner join attribute a on ca.attributeName = a.name inner join type t on a.typeid = t.id;",
                 ExecuteType.Query))
                {
                    foreach(DataRow row in table.Rows)
                    {
                        if (caseId.Equals(Convert.ToInt32(row[0])) && name.Equals(Convert.ToString(row[1])))
                        {
                            cases[cases.Count - 1].AddAttribute(Convert.ToString(row[2]), Convert.ToString(row[10]));
                        }
                        else
                        {
                            caseId = Convert.ToInt32(row[0]);
                            name = Convert.ToString(row[1]);
                            Case newCase = new Case(caseId, name);
                            newCase.AddAttribute(Convert.ToString(row[2]), Convert.ToString(row[10]));
                            cases.Add(newCase);

                            if (!newCase.CheckCaseInput(newCase, _cacheAttributes))
                            {
                                _logger.InsertLog(DbLogger.LoggingEvents.ERROR.ToString(), "Couldn't get all the cases at PostgreSQLCaseRepo.GetAll() because one of the cases doesn't have a correct value.");
                                CloseConnection();
                                return null;
                            }
                        }
                    }
                    CloseConnection();
                }
                _logger.InsertLog(DbLogger.LoggingEvents.INFORMATION.ToString(), "Successfully got all the cases at PostgreSQLCaseRepo.GetAll().");
            }
            catch (NpgsqlException e)
            {
                _logger.InsertLog(DbLogger.LoggingEvents.ERROR.ToString(), $"Couldnt get all the cases at PostgreSQLCaseRepo.GetAll() because an error occurred with stacktrace: {e.Message}");
            }
            finally
            {
                CloseConnection();
            }

            return cases;
        }

        /// <summary>
        /// Method to get a case by the Id.
        /// </summary>
        /// <param name="id">The id of the case that needs to be found.</param>
        /// <returns>The found case by the id.</returns>
        public Case GetById(int id)
        {
            bool first = true;
            Case matchCase = null;

            List<NpgsqlParameter> parameters = new List<NpgsqlParameter>() {
                new NpgsqlParameter("id", id)
            };

            try
            {
                using (DataTable table = Execute("SELECT m.*, a.*, t.*, ca.input FROM Modelcase m inner join case_attribute ca on m.id = ca.caseid " +
                 "inner join attribute a on ca.attributeName = a.Name inner join type t on a.typeid = t.id WHERE m.id = @id;",
                 ExecuteType.Query, parameters))
                {
                    foreach(DataRow row in table.Rows)
                    {
                        if (first)
                        {
                            matchCase = new Case(Convert.ToInt32(row[0]), Convert.ToString(row[1]));
                            first = false;
                        }
                        matchCase.AddAttribute(Convert.ToString(row[2]), Convert.ToString(row[10]));
                    }

                    if (matchCase != null && (bool)!matchCase?.CheckCaseInput(matchCase, _cacheAttributes))
                    {
                        _logger.InsertLog(DbLogger.LoggingEvents.ERROR.ToString(), $"Couldn't get the case with id {id} at PostgreSQLCaseRepo.GetById() because the case does have incorrect values.");
                        CloseConnection();
                        return null;
                    }
                    _logger.InsertLog(DbLogger.LoggingEvents.INFORMATION.ToString(), $"Successfully got the case with id {id} at PostgreSQLCaseRepo.GetById().");
                    CloseConnection();
                }
            }
            catch (NpgsqlException e)
            {
                _logger.InsertLog(DbLogger.LoggingEvents.ERROR.ToString(), $"Couldn't get the case with id {id} at PostgreSQLCaseRepo.GetById() due to an error with stacktrace: {e.Message}");
            }
            finally
            {
                CloseConnection();
            }

            return matchCase;
        }

        /// <summary>
        /// Method to insert a new case inside the database.
        /// </summary>
        /// <param name="obj">The case that needs to be inserted into the database.</param>
        /// <returns>The newly inserted case.</returns>
        public Case Insert(Case obj)
        {
            List<NpgsqlParameter> parameters = new List<NpgsqlParameter>() {
                new NpgsqlParameter("name", obj.Name)
            };

            try
            {
                long caseId = ExecuteScalar("INSERT INTO Modelcase (id, name) VALUES (default, @name);", parameters);

                if (caseId == -1)
                {
                    return null;
                }

                foreach (KeyValuePair<string, string> kvp in obj.Attributes)
                {
                    List<NpgsqlParameter> parametersAttributes = new List<NpgsqlParameter>() {
                        new NpgsqlParameter("attributeName", kvp.Key),
                        new NpgsqlParameter("caseid", caseId),
                        new NpgsqlParameter("input", kvp.Value)
                    };

                    Execute("INSERT INTO case_attribute (id, attributeName, caseid, input) VALUES (default, @attributeName, @caseid, @input);",
                     ExecuteType.NonQuery, parametersAttributes);
                    CloseConnection();
                    _logger.InsertLog(DbLogger.LoggingEvents.INFORMATION.ToString(), "Successfully inserted a new case at PostgreSQLCaseRepo.Insert().");
                }

                if (obj.Attributes.Count <= 0)
                {
                    Execute("INSERT INTO case_attribute (id) VALUES (default);",
                     ExecuteType.NonQuery);
                    CloseConnection();
                    _logger.InsertLog(DbLogger.LoggingEvents.INFORMATION.ToString(), "Successfully inserted a new case at PostgreSQLCaseRepo.Insert().");
                }
            }
            catch (NpgsqlException e)
            {
                _logger.InsertLog(DbLogger.LoggingEvents.ERROR.ToString(), $"Couldn't insert the new case at PostgreSQLCaseRepo.Insert() due to an error with stacktrace: {e.Message}");
            }
            finally
            {
                CloseConnection();
            }

            return obj;
        }

        /// <summary>
        /// Method to remove a case from the database.
        /// </summary>
        /// <param name="obj">The case that needs to be removed from the database.</param>
        public void Remove(Case obj)
        {
            List<NpgsqlParameter> parameters = new List<NpgsqlParameter> {
                new NpgsqlParameter("id", obj.Id)
            };

            try
            {
                Execute("DELETE FROM case WEHRE id = @id;", ExecuteType.NonQuery, parameters);
                CloseConnection();
                _logger.InsertLog(DbLogger.LoggingEvents.INFORMATION.ToString(), $"Successfully removed case with id {obj.Id} at PostgreSQLCaseRepo.Remove()");
            }
            catch (NpgsqlException e)
            {
                _logger.InsertLog(DbLogger.LoggingEvents.ERROR.ToString(), $"Couldn't remove case with id {obj.Id} at PostgreSQLCaseRepo.Remove() due to an error with stacktrace: {e.Message}");
            }
            finally
            {
                CloseConnection();
            }
        }

        /// <summary>
        /// Method to update an exisiting case in the database.
        /// </summary>
        /// <param name="obj">The case that needs to be updated.</param>
        /// <returns>The updated case.</returns>
        public Case Update(Case obj)
        {
            List<NpgsqlParameter> parameters = new List<NpgsqlParameter>() {
                new NpgsqlParameter("name", obj.Name),
                new NpgsqlParameter("id", obj.Id)
            };

            try
            {
                Execute("UPDATE Modelcase SET name = @name WHERE id = @id;", ExecuteType.NonQuery,
                 parameters);
                CloseConnection();
                _logger.InsertLog(DbLogger.LoggingEvents.INFORMATION.ToString(), $"Successfully updated the name of case with id {obj.Id}.");

                foreach (KeyValuePair<string, string> kvp in obj.Attributes)
                {
                    List<NpgsqlParameter> parametersAttributes = new List<NpgsqlParameter>() {
                        new NpgsqlParameter("attributeName", kvp.Key),
                        new NpgsqlParameter("caseid", obj.Id),
                        new NpgsqlParameter("input", kvp.Value)
                    };

                    Execute("UPDATE case_attribute SET input = @input WHERE attributeName = @attributeName AND caseid = caseid;",
                     ExecuteType.NonQuery, parametersAttributes);
                    CloseConnection();
                    _logger.InsertLog(DbLogger.LoggingEvents.INFORMATION.ToString(), $"Successfully updated the case with id {obj.Id}.");
                }
            }
            catch (NpgsqlException e)
            {
                _logger.InsertLog(DbLogger.LoggingEvents.ERROR.ToString(), $"Couldn't update the case with id {obj.Id} because of an error with stacktrace: {e.Message}.");
            }
            finally
            {
                CloseConnection();
            }

            return obj;
        }
    }
}