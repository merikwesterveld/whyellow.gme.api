﻿using System;
using System.Collections.Generic;
using System.Data;
using Npgsql;
using Whyellow.GME.API.Datasources;
using Whyellow.GME.API.Interfaces;
using Whyellow.GME.API.IRepositories;
using Whyellow.GME.API.Models;
using Attribute = Whyellow.GME.API.Models.Attribute;

namespace Whyellow.GME.API.Data.PostgreSQL
{
    /// <summary>
    /// Class implementation for the postgresql attribute repository.
    /// </summary>
    // ReSharper disable once InconsistentNaming
    public class PostgreSQLAttributeRepo : PostgreSQLRepo, IAttributeRepo
    {
        /// <summary>
        /// Logger to log all the activities in the Application.
        /// </summary>
        private readonly ILogger _logger;

        /// <summary>
        /// Constructor to create an instance of the PostgreSQLAttribute repository implementation.
        /// </summary>
        /// <param name="logger">The logger used to log the activities.</param>
        public PostgreSQLAttributeRepo(ILogger logger, IDatabaseOptions databaseOptions) : base(databaseOptions)
        {
            _logger = logger;
        }

        /// <summary>
        /// Method to get all the attributes out of the database.
        /// </summary>
        /// <returns>A list of attributes from the database.</returns>
        public List<Attribute> GetAll()
        {
            List<Attribute> attributes = new List<Attribute>();

            try
            {
                using (DataTable table = Execute("SELECT a.*, t.* FROM Attribute a INNER JOIN type t ON t.id = a.typeid;",
                 ExecuteType.Query))
                {
                    foreach(DataRow row in table.Rows)
                    {
                        double weight = 1;
                        if (Convert.ToDouble(row[3]) > 0)
                        {
                            weight = Convert.ToDouble(row[3]);
                        }
                        Attribute attribute = new Attribute(Convert.ToString(row[0]), new AttributeType(Convert.ToInt32(row[4]),
                            Convert.ToString(row[5]), Convert.ToBoolean(row[6]), Convert.ToBoolean(row[7])), Convert.ToString(row[2]), weight);
                        if (attribute.CheckModel())
                        {
                            attributes.Add(attribute);
                        }
                        else
                        {
                            _logger.InsertLog(DbLogger.LoggingEvents.ERROR.ToString(), "Couldn't get all the attributes from PostgreSQLAttributeRepo.GetAll() because one of the attribute's isn't correct. ");
                            CloseConnection();
                            return null;
                        }
                    }
                    CloseConnection();
                    _logger.InsertLog(DbLogger.LoggingEvents.INFORMATION.ToString(), "Successfully got all the attributes from PostgreSQLAttributeRepo.GetAll().");
                }
            }
            catch (NpgsqlException e)
            {
                _logger.InsertLog(DbLogger.LoggingEvents.ERROR.ToString(), $"Couldn\'t get all the attributes at PostgreSQLAttributeRepo.GetAll() due toe an error with stacktrace: {e.Message}");
            }
            finally
            {
                CloseConnection();
            }

            return attributes;
        }

        /// <summary>
        /// Method to get the attribute by the Id.
        /// </summary>
        /// <param name="id">The id of the attribute that needs to be found.</param>
        /// <returns>The found attribute.</returns>
        public Attribute GetById(string id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Method to get an attribute by the corresponding ID from the database.
        /// </summary>
        /// <param name="id">The ID that belongs to the attribute.</param>
        /// <returns>The found attribute from the database by the id.</returns>
        public Attribute GetById(int id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Method to insert an attribute into the database.
        /// </summary>
        /// <param name="obj">The attribute that needs to get inserted.</param>
        /// <returns>The inserted attribute.</returns>
        public Attribute Insert(Attribute obj)
        {
            try
            {
                Execute("INSERT INTO Attribute (typeid, name, settings, weight) VALUES (@typeid, @name, @settings, @weight)",
                 ExecuteType.NonQuery, CreateParameterList(obj));
                CloseConnection();
                _logger.InsertLog(DbLogger.LoggingEvents.INFORMATION.ToString(), "Successfully inserted a new attribute.");
            }
            catch (NpgsqlException e)
            {
                _logger.InsertLog(DbLogger.LoggingEvents.INFORMATION.ToString(), $"Couldnt insert a new attribute at PostgreSQLAttributeRepo.Insert() due to an error with stacktrace: {e.Message}.");
            }
            finally
            {
                CloseConnection();
            }

            return obj;
        }

        /// <summary>
        /// Method to remove an attribute out of the database.
        /// </summary>
        /// <param name="obj">The attribute that needs to get removed.</param>
        public void Remove(Attribute obj)
        {
            List<NpgsqlParameter> parameters = new List<NpgsqlParameter> {
                new NpgsqlParameter("name", obj.Name)
            };

            try
            {
                Execute("DELETE FROM Attribute WEHRE name = @name", ExecuteType.NonQuery, parameters);
                CloseConnection();
                _logger.InsertLog(DbLogger.LoggingEvents.INFORMATION.ToString(), $"Successfully removed attribute with name {obj.Name} at PostgreSQLAttributerepo.Remove().");
            }
            catch (NpgsqlException e)
            {
                _logger.InsertLog(DbLogger.LoggingEvents.ERROR.ToString(), $"Couldn\'t remove attribute with name {obj.Name} because of an error with stacktrace: {e.Message}");
            }
            finally
            {
                CloseConnection();
            }
        }

        /// <summary>
        /// Method to update an attribute in the database.
        /// </summary>
        /// <param name="obj">The updated attribute that needs to be updated in the database.</param>
        /// <returns>The updated attribute.</returns>
        public Attribute Update(Attribute obj)
        {
            try
            {
                Execute("UPDATE Attribute SET typeid = @t, name = @n, settings = @s, weight = @w WHERE name = @name",
                 ExecuteType.NonQuery, CreateParameterList(obj));
                CloseConnection();
                _logger.InsertLog(DbLogger.LoggingEvents.INFORMATION.ToString(), $"Successfully updated attribute with name {obj.Name} at PostgreSQLAttributeRepo.Update().");
            }
            catch (NpgsqlException e)
            {
                _logger.InsertLog(DbLogger.LoggingEvents.ERROR.ToString(), $"Couldn't update attribute with name {obj.Name} because an error occurred with stacktrace: {e.Message}.");
            }
            finally
            {
                CloseConnection();
            }

            return obj;
        }

        /// <summary>
        /// Method to get an attribute by their corresponding name.
        /// </summary>
        /// <param name="name">The name of the attribute that needs to be found in the database.</param>
        /// <returns>The found attribute by the name.</returns>
        public Attribute GetByName(string name)
        {
            List<NpgsqlParameter> parameters = new List<NpgsqlParameter> {
                new NpgsqlParameter("name", name)
            };

            try
            {
                using (DataTable table = Execute("SELECT a.*, t.* FROM Attribute a INNER JOIN type t ON t.id = a.typeid WHERE a.name = @name;",
                  ExecuteType.Query, parameters))
                {
                    foreach(DataRow row in table.Rows)
                    {
                        double weight = 1;
                        if ((double)row[3] > 0)
                        {
                            weight = (double)row[3];
                        }
                        Attribute attribute = new Attribute(Convert.ToString(row[0]), new AttributeType(Convert.ToInt32(row[4]),
                            Convert.ToString(row[5]), Convert.ToBoolean(row[6]), Convert.ToBoolean(row[7])), Convert.ToString(row[2]), weight);

                        if (!attribute.CheckModel())
                        {
                            _logger.InsertLog(DbLogger.LoggingEvents.ERROR.ToString(), $"Couldn't get the attribute by name {name} at PostgreSQLAttributeRepo.GetByName() because the attribute isn't correct.");
                            CloseConnection();
                            return null;
                        }

                        _logger.InsertLog(DbLogger.LoggingEvents.INFORMATION.ToString(), $"Successfully got the attribute by name {name} at PostgreSQLAttributeRepo.GetByName().");
                        CloseConnection();
                        return attribute;
                    }
                    CloseConnection();
                }
            }
            catch (NpgsqlException e)
            {
                _logger.InsertLog(DbLogger.LoggingEvents.ERROR.ToString(), $"Couldn't get attribute by name {name} because of an error that occurred with stacktrace: {e.Message}.");
            }
            finally
            {
                CloseConnection();
            }

            return null;
        }

        /// <summary>
        /// Method to create a list of parameters.
        /// </summary>
        /// <param name="obj">The attribute object that needs to be inserted.</param>
        /// <returns>The list with parameters.</returns>
        private List<NpgsqlParameter> CreateParameterList(Attribute obj)
        {
            List<NpgsqlParameter> parameters = new List<NpgsqlParameter> {
                new NpgsqlParameter("typeid", obj.Type.Id),
                new NpgsqlParameter("name", obj.Name),
                new NpgsqlParameter("weight", obj.Weight)
            };

            if (String.IsNullOrEmpty(obj.Settings))
            {
                parameters.Add(new NpgsqlParameter("settings", String.Empty));
            }
            else
            {
                parameters.Add(new NpgsqlParameter("settings", obj.Settings));
            }

            return parameters;
        }
    }
}