﻿using System;
using System.Collections.Generic;
using System.Data;
using Npgsql;
using Whyellow.GME.API.Datasources;
using Whyellow.GME.API.Interfaces;
using Whyellow.GME.API.IRepositories;
using Whyellow.GME.API.Models;

namespace Whyellow.GME.API.Data.PostgreSQL
{
    /// <summary>
    /// Class implementation for the appreciation repository with postgresql.
    /// </summary>
    public class PostgreSQLAppreciationRepo : PostgreSQLRepo, IAppreciationRepo
    {
        /// <summary>
        /// Logger to log all the activities in the application.
        /// </summary>
        private readonly ILogger _logger;

        /// <summary>
        /// Constructor to create an instance of the PostgreSQLAppreciationRepo implementation.
        /// </summary>
        /// <param name="logger">The logger used to log all the activities.</param>
        public PostgreSQLAppreciationRepo(ILogger logger, IDatabaseOptions databaseOptions) : base(databaseOptions)
        {
            _logger = logger;
        }

        /// <summary>
        /// Method to get all the appreciations from the postgresql database.
        /// </summary>
        /// <returns>A list of all the implementations found by the postgresql database.</returns>
        public List<Appreciation> GetAll()
        {
            List<Appreciation> appreciations = new List<Appreciation>();

            try
            {
                using (DataTable table = Execute("SELECT * FROM Appreciation;", ExecuteType.Query))
                {
                    foreach(DataRow row in table.Rows)
                    {
                        appreciations.Add(new Appreciation(Convert.ToInt32(row[0]), Convert.ToString(row[1]), Convert.ToDouble(row[3]),
                            (Appreciation.TypeAppreciation)Enum.Parse(typeof(Appreciation.TypeAppreciation), Convert.ToString(row[4])), Convert.ToInt32(row[2])));
                    }
                    CloseConnection();
                    _logger.InsertLog(DbLogger.LoggingEvents.INFORMATION.ToString(), "Successfully got all the appreciations from the database at PostgreSQLAppreciationRepo.GetAll().");
                }
            }
            catch (NpgsqlException e)
            {
                _logger.InsertLog(DbLogger.LoggingEvents.ERROR.ToString(), $"Couldn\'t get all the appreciations from the database at PostgreSQLAppreciationRepo.GetAll() because of an error that occurred with stacktrace: {e.Message}.");
            }
            finally
            {
                CloseConnection();
            }

            return appreciations;
        }

        /// <summary>
        /// Method to get an appreciation by the id.
        /// </summary>
        /// <param name="id">The Id of the appreciation that needs to be found.</param>
        /// <returns>The found appreciation by the id in the database.</returns>
        public Appreciation GetById(int id)
        {
            List<NpgsqlParameter> parameters = new List<NpgsqlParameter> {
                new NpgsqlParameter("id", id)
            };

            try
            {
                using (DataTable table = Execute("SELECT * FROM Appreciation WHERE id = @id", ExecuteType.Query, parameters))
                {
                    foreach(DataRow row in table.Rows)
                    {
                        return new Appreciation(Convert.ToInt32(row[0]), Convert.ToString(row[1]), Convert.ToDouble(row[3]),
                            (Appreciation.TypeAppreciation)Enum.Parse(typeof(Appreciation.TypeAppreciation), Convert.ToString(row[4])), Convert.ToInt32(row[2]));
                    }
                    CloseConnection();
                    _logger.InsertLog(DbLogger.LoggingEvents.INFORMATION.ToString(), $"Succesfully got the appreciation with id {id} at PostgreSQLAppreciationRepo.GetById().");
                }
            }
            catch (NpgsqlException e)
            {
                _logger.InsertLog(DbLogger.LoggingEvents.ERROR.ToString(), $"Couldn't get the appreciation with Id {id} because an error occurred with stacktrace: {e.Message}.");
            }
            finally
            {
                CloseConnection();
            }

            return null;
        }

        /// <summary>
        /// Method to insert an appreciation into the database.
        /// </summary>
        /// <param name="obj">The appreciation that needs to be inserted into the database.</param>
        /// <returns>The inserted appreciation.</returns>
        public Appreciation Insert(Appreciation obj)
        {
            List<NpgsqlParameter> parameters = new List<NpgsqlParameter> {
            new NpgsqlParameter("value", obj.Value),
            new NpgsqlParameter("profileid", obj.ProfileId),
            new NpgsqlParameter("amount", obj.Amount),
            new NpgsqlParameter("type", obj.Type)
        };

            try
            {
                long id = ExecuteScalar(
                 "INSERT INTO Appreciation (id, value, profileid, amount, type) VALUES (default, @value, @profileid, " +
                 "@amount, @type) RETURNING id;", parameters);
                CloseConnection();

                if (id == -1)
                {
                    return null;
                }

                List<NpgsqlParameter> parameters2 = new List<NpgsqlParameter> {
                    new NpgsqlParameter("profileid", obj.ProfileId),
                    new NpgsqlParameter("caseid", obj.CaseId)
                };

                setProfileLikedCase(parameters2);
                obj.Id = (int)id;
                _logger.InsertLog(DbLogger.LoggingEvents.INFORMATION.ToString(), "Successfully inserted an appreciation into the database at PostgreSQLAppreciationRepo.Insert(). ");
            }
            catch (NpgsqlException e)
            {
                _logger.InsertLog(DbLogger.LoggingEvents.ERROR.ToString(), $"Couldn\'t insert the appreciation at PostgreSQLAppreciationRepo.Insert() because an error occurred with stacktrace: {e.Message}");
            }
            finally
            {
                CloseConnection();
            }

            return obj;
        }

        /// <summary>
        /// Method to remove an appreciation from the database.
        /// </summary>
        /// <param name="obj">The appreciation that needs to be removed out of the databse.</param>
        public void Remove(Appreciation obj)
        {
            List<NpgsqlParameter> parameters = new List<NpgsqlParameter> {
                new NpgsqlParameter("id", obj.Id)
            };

            try
            {
                Execute("DELETE FROM Appreciation WEHRE id = @id", ExecuteType.NonQuery, parameters);
                CloseConnection();
                _logger.InsertLog(DbLogger.LoggingEvents.INFORMATION.ToString(), "Successfully removed the appreciation at PostgreSQLAppreciationRepo.Remove().");
            }
            catch (NpgsqlException e)
            {
                _logger.InsertLog(DbLogger.LoggingEvents.ERROR.ToString(), $"Couldn\'t remove the appreciation at PostgreSQLAppreciationRepo.Remove() due to an error with stacktrace: {e.Message}");
            }
            finally
            {
                CloseConnection();
            }
        }

        /// <summary>
        /// Method to set a profile liked case.
        /// </summary>
        /// <param name="parameters">The parameters used for this sql operation.</param>
        public void setProfileLikedCase(List<NpgsqlParameter> parameters)
        {
            try
            {
                Execute("INSERT INTO profile_case (id, profileid, caseid) VALUES (default, @profileid, @caseid);",
                    ExecuteType.NonQuery, parameters);
                CloseConnection();
            }
            catch (NpgsqlException e)
            {
                _logger.InsertLog(DbLogger.LoggingEvents.ERROR.ToString(),
                    $"Couldn't insert a profile like case because of an error with stacktrace: {e.Message}");
            }
            finally
            {
                CloseConnection();
            }
        }

        /// <summary>
        /// Method to update an appreciation from the database.
        /// </summary>
        /// <param name="obj">The appreciation that needs to be updated in the database.</param>
        /// <returns>The updated appreciation in the database.</returns>
        public Appreciation Update(Appreciation obj)
        {
            List<NpgsqlParameter> parameters = new List<NpgsqlParameter> {
                new NpgsqlParameter("value", obj.Value),
                new NpgsqlParameter("profileid", obj.ProfileId),
                new NpgsqlParameter("amount", obj.Amount),
                new NpgsqlParameter("type", obj.Type),
                new NpgsqlParameter("id", obj.Id)
            };

            try
            {
                Execute("UPDATE Appreciation SET value = @value, profileid = @profileid, amount = @amount, type = @type WHERE id = @id;", ExecuteType.NonQuery, parameters);

                List<NpgsqlParameter> parameters2 = new List<NpgsqlParameter> {
                    new NpgsqlParameter("profileid", obj.ProfileId),
                    new NpgsqlParameter("caseid", obj.CaseId)
                };

                CloseConnection();

                setProfileLikedCase(parameters2);

                _logger.InsertLog(DbLogger.LoggingEvents.INFORMATION.ToString(), $"Successfully updated an appreciation with Id {obj.Id} at PostgreSQLAppreciationRepo.Update().");
            }
            catch (NpgsqlException e)
            {
                _logger.InsertLog(DbLogger.LoggingEvents.ERROR.ToString(), $"Couldn't update the appriciation with Id {obj.Id} due to an error with stacktrace: {e.Message}.");
            }
            finally
            {
                CloseConnection();
            }

            return obj;
        }

        /// <summary>
        /// Method to get all appreciations by a profile id.
        /// </summary>
        /// <param name="id">The id of the profile that needs to get all the appreciations.</param>
        /// <returns>The list of appreciations by that profile.</returns>
        public List<Appreciation> GetAllByProfileId(int id)
        {
            List<NpgsqlParameter> parameters = new List<NpgsqlParameter> {
                new NpgsqlParameter("id", id)
            };
            List<Appreciation> appreciations = new List<Appreciation>();

            try
            {
                using (DataTable table = Execute("SELECT * FROM Appreciation WHERE profileID = @id",
                 ExecuteType.Query, parameters))
                {
                    foreach(DataRow row in table.Rows)
                    {
                        appreciations.Add(new Appreciation(Convert.ToInt32(row[0]), Convert.ToString(row[1]), Convert.ToInt32(row[3]),
                            (Appreciation.TypeAppreciation)Enum.Parse(typeof(Appreciation.TypeAppreciation), Convert.ToString(row[4])), Convert.ToInt32(row[2])));
                    }
                    _logger.InsertLog(DbLogger.LoggingEvents.INFORMATION.ToString(), $"Successfully got all the appreciations from the profile with id {id}.");
                    CloseConnection();
                }
            }
            catch (NpgsqlException e)
            {
                _logger.InsertLog(DbLogger.LoggingEvents.ERROR.ToString(), $"Couldn't get all the appreciations from the profile with Id {id} because an error occurred with stacktrace {e.Message}.");
            }
            finally
            {
                CloseConnection();
            }
            return appreciations;
        }
    }
}