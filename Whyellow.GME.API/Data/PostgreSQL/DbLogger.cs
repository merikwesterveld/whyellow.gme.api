﻿using System;
using System.Collections.Generic;
using Npgsql;
using Whyellow.GME.API.Datasources;
using Whyellow.GME.API.Interfaces;
using Whyellow.GME.API.IRepositories;

namespace Whyellow.GME.API.Data.PostgreSQL
{
    /// <summary>
    /// Class to log all the activities in the database.
    /// </summary>
    public class DbLogger : PostgreSQLRepo, ILogger
    {
        /// <summary>
        /// Enum used to log what kind of event happened. 
        /// </summary>
        public enum LoggingEvents
        {
            /// <summary>
            /// Logging event error which indicates that an heavy error occurred.
            /// </summary>
            ERROR,
            /// <summary>
            /// Logging event information which indicates that something less important happened.
            /// </summary>
            INFORMATION,
            /// <summary>
            /// Logging even warning which indicates that something might have gone wrong but that isn't true for everything.
            /// </summary>
            WARNING,
        }

        /// <summary>
        /// Method to insert a log into the database.
        /// </summary>
        /// <param name="logLevel">the log level / log event of the log.</param>
        /// <param name="message">The supporting message of the log.</param>
        public void InsertLog(string logLevel, string message)
        {
                List<NpgsqlParameter> parameters = new List<NpgsqlParameter>()
                {
                    new NpgsqlParameter("logLevel", logLevel),
                    new NpgsqlParameter("message", message),
                    new NpgsqlParameter("createdTime", DateTime.Now)
                };

                Execute(
                    "INSERT INTO eventlog (ID, LogLevel, Message, CreatedTime) VALUES (default, @logLevel, @message, @createdTime);",
                    ExecuteType.NonQuery, parameters);
                CloseConnection();
        }

        public DbLogger(IDatabaseOptions databaseOptions) : base(databaseOptions)
        {
            
        }
    }
}
