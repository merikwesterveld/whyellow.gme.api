﻿using System;
using System.Collections.Generic;
using System.Data;
using Npgsql;
using Whyellow.GME.API.Datasources;
using Whyellow.GME.API.Interfaces;
using Whyellow.GME.API.IRepositories;
using Whyellow.GME.API.Models;

namespace Whyellow.GME.API.Data.PostgreSQL
{
    /// <summary>
    /// Class implementation for the postgresql attributetype repository.
    /// </summary>
    public class PostgreSQLAttributeTypeRepo : PostgreSQLRepo, IAttributeTypeRepo
    {
        /// <summary>
        /// Logger to log all the activities in the application.
        /// </summary>
        private readonly ILogger _logger;

        /// <summary>
        /// Constructor to instanciate the PostgreSQLAttributeRepo implementation.
        /// </summary>
        /// <param name="logger"></param>
        public PostgreSQLAttributeTypeRepo(ILogger logger, IDatabaseOptions databaseOptions) : base(databaseOptions)
        {
            _logger = logger;
        }

        /// <summary>
        /// Method to get all the attributetypes from the database.
        /// </summary>
        /// <returns>A list of all the attributetypes found in the database.</returns>
        public List<AttributeType> GetAll()
        {
            List<AttributeType> attributeTypes = new List<AttributeType>();

            try
            {
                using (DataTable table = Execute("SELECT * FROM type;", ExecuteType.Query))
                {
                    foreach(DataRow row in table.Rows)
                    {
                        attributeTypes.Add(new AttributeType(Convert.ToInt32(row[0]), Convert.ToString(row[1]),
                            Convert.ToBoolean(row[2]), Convert.ToBoolean(row[3])));
                    }
                    _logger.InsertLog(DbLogger.LoggingEvents.INFORMATION.ToString(), "Successfully got all the attribute types from the database at PostgreSQLAttributeTypeRepo.GetAll().");
                    CloseConnection();
                }
            }
            catch (NpgsqlException e)
            {
                _logger.InsertLog(DbLogger.LoggingEvents.ERROR.ToString(), $"Couldn't get all the attributetypes from the datasource because an error occurred with stacktrace: {e.Message}.");
            }
            finally
            {
                CloseConnection();
            }

            return attributeTypes;
        }

        /// <summary>
        /// Method to get an attributetype by id.
        /// </summary>
        /// <param name="id">The id of the attributetype.</param>
        /// <returns>The attributetype found by the Id.</returns>
        public AttributeType GetById(int id)
        {
            List<NpgsqlParameter> parameters = new List<NpgsqlParameter>() {
                new NpgsqlParameter("id", id)
            };

            try
            {
                using (DataTable table = Execute("SELECT * FROM type WHERE id = @id;", ExecuteType.Query,
                 parameters))
                {
                    foreach(DataRow row in table.Rows)
                    {
                        return new AttributeType(Convert.ToInt32(row[0]), Convert.ToString(row[1]), Convert.ToBoolean(row[2]),
                            Convert.ToBoolean(row[3]));
                    }
                    _logger.InsertLog(DbLogger.LoggingEvents.INFORMATION.ToString(), $"Successfully got the attribute with id {id} from the database at PostgreSQLAttributeTypeRepo.GetById().");
                    CloseConnection();
                }
            }
            catch (NpgsqlException e)
            {
                _logger.InsertLog(DbLogger.LoggingEvents.ERROR.ToString(), $"Couldn't get the attribute with id {id} because of an error that occurred at PostgreSQLAttributeTypeRepo.GetById() with stacktrace: {e.Message}");
            }
            finally
            {
                CloseConnection();
            }

            return null;
        }

        /// <summary>
        /// Method to insert a new attributetype into the postgresql database.
        /// </summary>
        /// <param name="obj">The new attributetype that needs to get inserted.</param>
        /// <returns>The inserted attributetype.</returns>
        public AttributeType Insert(AttributeType obj)
        {
            List<NpgsqlParameter> parameters = new List<NpgsqlParameter>() {
                new NpgsqlParameter("abbreviation", obj.Abbreviation),
                new NpgsqlParameter("multiple_values", obj.HasMultipleValues),
                new NpgsqlParameter("numeric_value", obj.IsNumericValue)
            };

            try
            {
                Execute("INSERT INTO Type (id, abbreviation, multiple_values, numeric_value) VALUES (default, @abbreviation, @multiple_values, @numeric_value)", ExecuteType.NonQuery, parameters);
                _logger.InsertLog(DbLogger.LoggingEvents.INFORMATION.ToString(), "Successfully inserted a new attributetype into the database.");
                CloseConnection();
            }
            catch (NpgsqlException e)
            {
                _logger.InsertLog(DbLogger.LoggingEvents.ERROR.ToString(), $"Couldn't insert the new attributye type into the database because of an error with stacktrace: {e.Message}.");
            }
            finally
            {
                CloseConnection();
            }

            return obj;
        }

        /// <summary>
        /// Method to remove an attributetype from the database.
        /// </summary>
        /// <param name="obj">The attributetype that needs to be removed.</param>
        public void Remove(AttributeType obj)
        {
            List<NpgsqlParameter> parameters = new List<NpgsqlParameter> {
                new NpgsqlParameter("id", obj.Id)
            };

            try
            {
                Execute("DELETE FROM type WEHRE id = @id", ExecuteType.NonQuery, parameters);
                CloseConnection();
                _logger.InsertLog(DbLogger.LoggingEvents.INFORMATION.ToString(), $"Successfully removed the attributeType with Id {obj.Id} at PostgreSQLAttributeTypeRepo.Remove().");
            }
            catch (NpgsqlException e)
            {
                _logger.InsertLog(DbLogger.LoggingEvents.ERROR.ToString(), $"Couldn't remove attributetype with Id {obj.Id} because of an error at PostgreSQLAttributeRepo.Remove() with stacktrace: {e.Message}");
            }
            finally
            {
                CloseConnection();
            }
        }

        /// <summary>
        /// Method to update an attributetype in the database.
        /// </summary>
        /// <param name="obj">The attributetype that needs to get updated in the database.</param>
        /// <returns>The updated attributetype.</returns>
        public AttributeType Update(AttributeType obj)
        {
            List<NpgsqlParameter> parameters = new List<NpgsqlParameter>() {
                new NpgsqlParameter("abbreviation", obj.Abbreviation),
                new NpgsqlParameter("multiple_values", obj.HasMultipleValues),
                new NpgsqlParameter("numeric_value", obj.HasMultipleValues),
                new NpgsqlParameter("id", obj.Id)
            };

            try
            {
                Execute("UPDATE Type SET abbreviation = @abbreviation, multiple_values = @multiple_values, numeric_value = " +
                 "@numeric_value WHERE id = @id", ExecuteType.NonQuery, parameters);
                CloseConnection();
                _logger.InsertLog(DbLogger.LoggingEvents.INFORMATION.ToString(), $"Successfully updated attributetype with Id {obj.Id} at PostgreSQLAttributeTypeRepo.Update().");
            }
            catch (NpgsqlException e)
            {
                _logger.InsertLog(DbLogger.LoggingEvents.ERROR.ToString(), $"Couldn't update attributetype with Id {obj.Id} at PostgreSQLAttributeTypeRepo.Update() because of an error with stacktrace: {e.Message}");
            }
            finally
            {
                CloseConnection();
            }

            return obj;
        }
    }
}