﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.VisualStudio.Web.CodeGenerators.Mvc.Razor;
using Npgsql;
using Whyellow.GME.API.Datasources;
using Whyellow.GME.API.Interfaces;
using Whyellow.GME.API.IRepositories;
using Whyellow.GME.API.Models;
using Attribute = Whyellow.GME.API.Models.Attribute;

namespace Whyellow.GME.API.Data.PostgreSQL
{
    /// <summary>
    /// Class implementation for the postgresql profile repository.
    /// </summary>
    public class PostgreSQLProfileRepo : PostgreSQLRepo, IProfileRepo
    {
        /// <summary>
        /// Logger used to log all the activities in the application.
        /// </summary>
        private readonly ILogger _logger;

        /// <summary>
        /// Attribute repository implementation used by this repository.
        /// </summary>
        private readonly PostgreSQLAttributeRepo _attributeRepo;

        /// <summary>
        /// Appreciation repository implementation used by this repository.
        /// </summary>
        private readonly PostgreSQLAppreciationRepo _appreciationRepo;

        private readonly List<Attribute> _cacheAttributes = new List<Attribute>();

        /// <summary>
        /// Constructor to instanciate this repository.
        /// </summary>
        /// <param name="logger">The logger used in this repo to log all the activities.</param>
        /// <param name="memoryCache">Memory cache used for the cache throughout the application.</param>
        public PostgreSQLProfileRepo(ILogger logger, IMemoryCache memoryCache, IDatabaseOptions databaseOptions) : base(databaseOptions)
        {
            _attributeRepo = new PostgreSQLAttributeRepo(logger, databaseOptions);
            _appreciationRepo = new PostgreSQLAppreciationRepo(logger, databaseOptions);
            _logger = logger;

            if (!memoryCache.TryGetValue("Attributes", out _cacheAttributes) && _attributeRepo != null)
            {
                _cacheAttributes = _attributeRepo.GetAll();
                memoryCache.Set("Attributes", _cacheAttributes);
            }
        }

        /// <summary>
        /// Method to get all the profiles from the postgresql database.
        /// </summary>
        /// <returns>A list of all the profiles present in the database.</returns>
        public List<Profile> GetAll()
        {
            List<Profile> profiles = new List<Profile>();

            int profileId = -1;

            try
            {
                using (DataTable table = Execute(
                    "SELECT p.*, a.*, t.*, pa.input FROM profile p inner join profile_attribute pa on p.id = pa.profileid " +
                    "inner join attribute a on pa.attributeName = a.Name inner join type t on a.typeid = t.id;",
                    ExecuteType.Query))
                {
                    foreach (DataRow row in table.Rows)
                    {
                        if (profileId.Equals((int) row[0]))
                        {
                            profiles[profiles.Count - 1].AddAttribute(Convert.ToString(row[2]), Convert.ToString(row[10]));
                        }
                        else
                        {
                            profileId = Convert.ToInt32(row[0]);
                            Profile profile = new Profile(profileId);
                            profile.AddAttribute(Convert.ToString(row[2]), Convert.ToString(row[10]));
                            profile.authId = Convert.ToString(row[1]);
                            profiles.Add(profile);

                            if (!profile.CheckProfileInput(profile, _cacheAttributes))
                            {
                                _logger.InsertLog(DbLogger.LoggingEvents.ERROR.ToString(),
                                    "Couldn't get all the profiles at PostgreSQLProfileRepo.GetAll() because one of the profile attribute values is incorrect.");
                                CloseConnection();
                                return null;
                            }
                        }
                    }
                    CloseConnection();
                }

                using (DataTable table = Execute("SELECT * FROM profile;", ExecuteType.Query))
                {
                    foreach (DataRow row in table.Rows)
                    {
                        int id = Convert.ToInt32(row[0]);
                        if (profiles.All(p => p.Id != id))
                        {
                            Profile profile = new Profile(id) {authId = Convert.ToString(row[1]) };
                            profiles.Add(profile);
                        }
                    }
                    CloseConnection();
                }
 
                foreach (Profile profile in profiles)
                {
                    profile.LikesAndDislikes.Add(_appreciationRepo.GetById(profile.Id));
                }
                CloseConnection();
                _logger.InsertLog(DbLogger.LoggingEvents.INFORMATION.ToString(),
                    "Successfully got all the profiles at PostgreSQLProfileRepo.GetAll().");
            }
            catch (NpgsqlException e)
            {
                _logger.InsertLog(DbLogger.LoggingEvents.ERROR.ToString(),
                    $"Couldn't get all the profiles at PostgreSQLProfileRepo.GetAll() due to an error with stacktrace: {e.Message}.");
            }
            finally
            {
                CloseConnection();    
            }

            return profiles;
        }

        /// <summary>
        /// Method to get a profily by the id.
        /// </summary>
        /// <param name="id">The id of the profile that needs to be found.</param>
        /// <returns>The found profile by the id.</returns>
        public Profile GetById(int id)
        {
            Profile profile = null;
            bool first = true;

            List<NpgsqlParameter> parameters = new List<NpgsqlParameter>() {
                new NpgsqlParameter("id", id)
            };

            try
            {
                using (DataTable table = Execute("SELECT p.*, a.name, pa.input FROM profile p left outer join profile_attribute pa on p.id = pa.profileid left outer join attribute a on pa.attributeName = a.Name left outer join type t on a.typeid = t.id WHERE p.id = @id",
                 ExecuteType.Query, parameters))
                {
                    foreach(DataRow row in table.Rows) {
                        if (first)
                        {
                            profile = new Profile(Convert.ToInt32(row[0])) { authId = Convert.ToString(row[1]) };
                            first = false;
                        }
                        if (!String.IsNullOrEmpty(Convert.ToString(row[2])) &&
                            !String.IsNullOrEmpty(Convert.ToString(row[3])))
                        {
                            profile.AddAttribute(Convert.ToString(row[2]), Convert.ToString(row[3]));
                        }
                    }
                    CloseConnection();

                    if (profile != null && !profile.CheckProfileInput(profile, _cacheAttributes))
                    {
                        _logger.InsertLog(DbLogger.LoggingEvents.ERROR.ToString(), $"Couldn't get the profile at PostgreSQLProfileRepo.GetById() with Id {id} because the attribute value's of the profile aren't correct.");
                        CloseConnection();
                        return null;
                    }
                }

                if (profile != null)
                {
                    profile.LikesAndDislikes.AddRange(_appreciationRepo.GetAllByProfileId(profile.Id));
                    CloseConnection();
                    _logger.InsertLog(DbLogger.LoggingEvents.INFORMATION.ToString(), $"Successfully found the profile at PostgreSQLProfileRepo.GetById() with Id {id}.");
                    return profile;
                }
            }
            catch (NpgsqlException e)
            {
                _logger.InsertLog(DbLogger.LoggingEvents.ERROR.ToString(), $"Couldn't get the profile at PostgreSQLProfileRepo.GetByID() with id {id} because an error occurred with stacktrace: {e.Message}");
            }
            finally
            {
                CloseConnection();
            }

            return null;
        }

        public Profile GetByAuthId(string id)
        {
            Profile profile = null;
            bool first = true;

            List<NpgsqlParameter> parameters = new List<NpgsqlParameter>() {
                new NpgsqlParameter("id", id)
            };

            try
            {
                using (DataTable table = Execute("SELECT p.*, a.name, pa.input FROM profile p left outer join profile_attribute pa on p.id = pa.profileid left outer join attribute a on pa.attributeName = a.Name left outer join type t on a.typeid = t.id WHERE p.authid = @id",
                 ExecuteType.Query, parameters))
                {
                    foreach(DataRow row in table.Rows)
                    {
                        if (first)
                        {
                            profile = new Profile(Convert.ToInt32(row[0])) { authId = Convert.ToString(row[1]) };
                            first = false;
                        }
                        if (!String.IsNullOrEmpty(Convert.ToString(row[2])) &&
                            !String.IsNullOrEmpty(Convert.ToString(row[3])))
                        {
                            profile.AddAttribute(Convert.ToString(row[2]), Convert.ToString(row[3]));
                        } 
                    }

                    if (profile != null && !profile.CheckProfileInput(profile, _cacheAttributes))
                    {
                        _logger.InsertLog(DbLogger.LoggingEvents.ERROR.ToString(), $"Couldn't get the profile at PostgreSQLProfileRepo.GetById() with Id {id} because the attribute value's of the profile aren't correct.");
                        return null;
                    }
                }

                if (profile != null)
                {
                    profile.LikesAndDislikes.AddRange(_appreciationRepo.GetAllByProfileId(profile.Id));
                    CloseConnection();
                    _logger.InsertLog(DbLogger.LoggingEvents.INFORMATION.ToString(), $"Successfully found the profile at PostgreSQLProfileRepo.GetById() with Id {id}.");
                    return profile;
                }
            }
            catch (NpgsqlException e)
            {
                _logger.InsertLog(DbLogger.LoggingEvents.ERROR.ToString(), $"Couldn't get the profile at PostgreSQLProfileRepo.GetByID() with id {id} because an error occurred with stacktrace: {e.Message}");
            }
            finally
            {
                CloseConnection();
            }

            return null;
        }

        /// <summary>
        /// Method to insert a new profile into the database.
        /// </summary>
        /// <param name="obj">The profile that needs to get inserted into the postgresql database.</param>
        /// <returns>The inserted profile.</returns>
        public Profile Insert(Profile obj)
        {
            List<NpgsqlParameter> parameters = new List<NpgsqlParameter>() {
                new NpgsqlParameter("authid", obj.authId)
            };

            try
            {
                long id = ExecuteScalar("INSERT INTO Profile (id, authid) VALUES (default, @authid) RETURNING id;",
                 parameters);
                CloseConnection();

                if (id == -1)
                {
                    return null;
                }

                foreach (KeyValuePair<string, string> kvp in obj.Attributes)
                {
                    List<NpgsqlParameter> parametersAttributes = new List<NpgsqlParameter>() {
                        new NpgsqlParameter("attributeName", kvp.Key),
                        new NpgsqlParameter("profileid", id),
                        new NpgsqlParameter("input", kvp.Value)
                };

                    Execute("INSERT INTO profile_attribute (id, attributeName, profileid, input) VALUES (default, @attributeName, @profileid, @input);",
                     ExecuteType.NonQuery, parametersAttributes);
                    CloseConnection();
                }
                _logger.InsertLog(DbLogger.LoggingEvents.INFORMATION.ToString(), $"Successfully posted a new profile with Id {id} at PostgreSQLProfileRepo.Insert().");
            }
            catch (NpgsqlException e)
            {
                _logger.InsertLog(DbLogger.LoggingEvents.ERROR.ToString(), $"Couldn't insert a new profile at PostgreSQLProfileRepo.Insert() because an error occurred with stacktrace {e.Message}");
            }
            finally
            {
                CloseConnection();
            }

            return obj;
        }

        /// <summary>
        /// Method to remove an existing profile out of the database.
        /// </summary>
        /// <param name="obj">The profile that needs to get removed.</param>
        public void Remove(Profile obj)
        {
            List<NpgsqlParameter> parameters = new List<NpgsqlParameter> {
                new NpgsqlParameter("id", obj.Id)
            };

            try
            {
                Execute("DELETE FROM profile WEHRE id = @id;", ExecuteType.NonQuery, parameters);
                CloseConnection();
                _logger.InsertLog(DbLogger.LoggingEvents.INFORMATION.ToString(), $"Successfully removed profile with id {obj.Id} at PostgreSQLProfileRepo.Remove().");
            }
            catch (NpgsqlException e)
            {
                _logger.InsertLog(DbLogger.LoggingEvents.ERROR.ToString(), $"Couldn't remove profile with id {obj.Id} at ProfileSQLProfileRepo.Remove() because an error occurred with stacktrace: {e.Message}.");
            }
            finally
            {
                CloseConnection();
            }
        }

        /// <summary>
        /// Method to update a profile in the database.
        /// </summary>
        /// <param name="obj">The profile that needs to get updated.</param>
        /// <returns>The updated profile.</returns>
        public Profile Update(Profile obj)
        {
            Profile dbProfile = GetById(obj.Id);

            foreach (KeyValuePair<string, string> kvp in obj.Attributes)
            {

                List<NpgsqlParameter> parametersAttributes = new List<NpgsqlParameter>() {
                    new NpgsqlParameter("attributeName", kvp.Key),
                    new NpgsqlParameter("profileid", obj.Id),
                    new NpgsqlParameter("input", kvp.Value)
                };

                try
                {
                    if (dbProfile.Attributes.Count > 0)
                    {
                        Execute("UPDATE profile_attribute SET input = @input WHERE attributeName = @attributeName AND profileid = @profileid;",
                         ExecuteType.NonQuery, parametersAttributes);
                    }
                    else
                    {
                        Execute("INSERT INTO profile_attribute (id, attributeName, profileid, input) VALUES (default, @attributeName, @profileid, @input);",
                         ExecuteType.NonQuery, parametersAttributes);
                    }
                    CloseConnection();
                    _logger.InsertLog(DbLogger.LoggingEvents.INFORMATION.ToString(), $"Successfully updated the profile with id {obj.Id} at PostgreSQLProfileRepo.Update().");
                }
                catch (NpgsqlException e)
                {
                    _logger.InsertLog(DbLogger.LoggingEvents.ERROR.ToString(), $"Couldn't update the profile with id {obj.Id} at PostgreSQLProfileRepo.Update() because an error occurred with stacktrace: {e.Message}.");
                }
                finally
                {
                    CloseConnection();
                }
            }

            return obj;
        }

        /// <summary>
        /// Method to get the liked cases from the database.
        /// </summary>
        /// <param name="id">The ID of the profile.</param>
        /// <returns>The list of the liked cases.</returns>
        public List<Case> GetLikedCases(int id)
        {
            List<Case> cases = new List<Case>();

            List<NpgsqlParameter> parameters = new List<NpgsqlParameter>() {
                new NpgsqlParameter("profileId", id)
            };

            try
            {
                using (DataTable table = Execute("SELECT c.* FROM Profile_case pc inner join modelcase c on pc.caseid = c.id WHERE ProfileID = @profileId",
                 ExecuteType.Query, parameters))
                {
                    foreach(DataRow row in table.Rows)
                    {
                        cases.Add(new Case(Convert.ToInt32(row[0]), Convert.ToString(row[1])));
                    }
                    CloseConnection();
                    _logger.InsertLog(DbLogger.LoggingEvents.INFORMATION.ToString(), $"Successfully got all the liked cases with ProfileId {id} at PostgreSQLProfileRepo.GetLikedCases().");
                }
            }
            catch (NpgsqlException e)
            {
                _logger.InsertLog(DbLogger.LoggingEvents.ERROR.ToString(), $"Couldn't get all the liked cases with ProfileId {id} at PostgreSQLProfileRepo.GetLikedCases() because an error occurred with stacktrace: {e.Message}.");
                return null;
            }
            finally
            {
                CloseConnection();
            }

            return cases;
        }
    }
}