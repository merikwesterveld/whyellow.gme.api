﻿using System.Collections.Generic;

namespace Whyellow.GME.API.Generic_Repo
{
    /// <summary>
    /// Generic repository interface for all the repositories.
    /// </summary>
    /// <typeparam name="T">The class that needs to be implemented.</typeparam>
    public interface IRepository<T> where T : class
    {
        /// <summary>
        /// Method to get all the objects from the datasource.
        /// </summary>
        /// <returns>A list of objects from the datasource.</returns>
        List<T> GetAll();
        /// <summary>
        /// Method to get an object by id from the datasource.
        /// </summary>
        /// <param name="id">The id of the object that needs to be get.</param>
        /// <returns>The object by the id.</returns>
        T GetById(int id);
        /// <summary>
        /// Method to insert an object into the datasource.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        T Insert(T obj);
        /// <summary>
        /// Method to remove an object from the datasource.
        /// </summary>
        /// <param name="obj">The object that needs to get removed.</param>
        void Remove(T obj);
        /// <summary>
        /// Method to update an object in the datasource.
        /// </summary>
        /// <param name="obj">The updated object.</param>
        /// <returns>The updated object.</returns>
        T Update(T obj);
    }
}
