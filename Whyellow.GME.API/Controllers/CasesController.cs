﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Whyellow.GME.API.Data.PostgreSQL;
using Whyellow.GME.API.IRepositories;
using Whyellow.GME.API.Models;
using ILogger = Whyellow.GME.API.IRepositories.ILogger;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Whyellow.GME.API.Controllers
{
    /// <summary>
    /// Controller to handle the case requests.
    /// </summary>
    [Route("api/[controller]")]
    // ReSharper disable once InheritdocConsiderUsage
    public class CasesController : Controller
    {
        /// <summary>
        /// Data repository used by the case controller.
        /// </summary>
        private readonly ICaseRepo _caseRepo;

        /// <summary>
        /// Data repository for the attribute repo used by the case controller.
        /// </summary>
        private readonly IAttributeRepo _attributeRepo;

        /// <summary>
        /// Data repository for the appreciation repo used by the case controller.
        /// </summary>
        private readonly IAppreciationRepo _appreciationRepo;

        /// <summary>
        /// Data repository for the profile repo used by the case controller.
        /// </summary>
        private readonly IProfileRepo _profileRepo;

        /// <summary>
        /// Logic class used by the case controller to handle the logic.
        /// </summary>
        private readonly Logic _logic;

        /// <summary>
        /// Logger used throughout the whole application to log the activities.
        /// </summary>
        private readonly ILogger _logger;

        /// <summary>
        /// List of cases from the cache used by this controller.
        /// </summary>
        private readonly List<Case> _cacheCases;

        private readonly IMemoryCache _memoryCache;

        /// <summary>
        /// List of attributes from the cahce used by this controller.
        /// </summary>
        private readonly List<Attribute> _cacheAttributes;

        /// <summary>
        /// Constructor to instantiate the case controller/
        /// </summary>
        /// <param name="caseRepo">The repository used by the case controller/</param>
        /// <param name="attributeRepo">The attribute repository used by the case controller.</param>
        /// <param name="appreciationRepo">The appreciation repository used by the case controller.</param>
        /// <param name="profileRepo">The profile repository used by the cas controller.</param>
        /// <param name="memoryCache">The used memory cache.</param>
        /// <param name="logger">Logger that is used to log all the events of the application.</param>
        public CasesController(ICaseRepo caseRepo, IAttributeRepo attributeRepo, IAppreciationRepo appreciationRepo, IProfileRepo profileRepo, IMemoryCache memoryCache, ILogger logger)
        {
            _logger = logger;
            _caseRepo = caseRepo;
            _attributeRepo = attributeRepo;
            _appreciationRepo = appreciationRepo;
            _profileRepo = profileRepo;
            _logic = new Logic();
            _cacheCases = new List<Case>();
            _cacheAttributes = new List<Attribute>();
            _memoryCache = memoryCache;

            if (!memoryCache.TryGetValue("Cases", out _cacheCases) && _caseRepo != null)
            {
                _cacheCases = _caseRepo.GetAll();
                memoryCache.Set("Cases", _cacheCases);
            } 

            if (!memoryCache.TryGetValue("Attributes", out _cacheAttributes) && _attributeRepo != null)
            {
                _cacheAttributes = _attributeRepo.GetAll();
                memoryCache.Set("Attributes", _cacheAttributes);
            }
        }

        /// <summary>
        /// Method to get all the cases from the datasource. 
        /// This can only be used when the user is logged in and the correct bearer token is being set into the header.
        /// </summary>
        /// <returns>A list of cases from the data source.</returns>
        /// <response code ="200">Ok when at least 1 case can be found.</response>
        /// <response code ="404">NotFound when 0 cases have been found.</response>
        [HttpGet]
        [Produces("application/json", Type = typeof(List<Case>))]
        [Authorize]
        public IActionResult Get()
        {
            if (_cacheCases != null)
            {
                _logger.InsertLog(DbLogger.LoggingEvents.INFORMATION.ToString(), "Got all the cases successfully at CasesController.Get().");
                return Ok(_cacheCases);
            }
            _logger.InsertLog(DbLogger.LoggingEvents.WARNING.ToString(), "Couldn't get any cases at CasesController.Get(). This could mean that there are no cases in the datasource or that an error occurred when getting cases from the datasource.");
            return NotFound();
        }

        /// <summary>
        /// Method to get a case by his ID.
        /// This can only be used when the user is logged in and the correct bearer token is being set into the header.
        /// </summary>
        /// <param name="caseId">The ID of the case that needs to be found.</param>
        /// <returns>The found case by the ID.</returns>
        /// <response code ="200">Ok when the case with the corresponding ID can be found.</response>
        /// <response code ="404">NotFound when the case with the ID can't be found in the datasource.</response>
        [HttpGet("{caseId}")]
        [Produces("application/json", Type = typeof(Case))]
        [Authorize]
        public IActionResult GetById(int caseId)
        {
            Case matchCase = _cacheCases.FirstOrDefault(c => c.Id.Equals(caseId));

            if (matchCase != null)
            {
                _logger.InsertLog(DbLogger.LoggingEvents.INFORMATION.ToString(), $" Got a case successfully from the datasource at CasesController.GetById() with the ID being {caseId}.");
                return Ok(matchCase);
            }
            _logger.InsertLog(DbLogger.LoggingEvents.WARNING.ToString(), $"Couldn't get the case from the datasource at CasesController.GetByID() with the case being {caseId}. This could mean that there is no case with this ID or that an error occurred.");
            return NotFound();
        }

        [HttpGet("likedCases/{profileId}")]
        [Produces("application/json", Type = typeof(List<Case>))]
        [Authorize]
        public IActionResult GetLikedCases(int profileId)
        {
            List<Case> likedCases = _profileRepo.GetLikedCases(profileId);
            List<Case> completeCases = new List<Case>();
            foreach (Case likedCase in likedCases)
            {
                completeCases.Add(_cacheCases.FirstOrDefault(c => c.Id.Equals(likedCase.Id)));
            }
            return Ok(completeCases);
        }

        /// <summary>
        /// Method to give a like to a case by a profile.
        /// This can only be used when the user is logged in and the correct bearer token is being set into the header
        /// and the logged in user is the same user that wants to give the like.
        /// </summary>
        /// <param name="caseId">The ID of the case that has to receive a like from the profile.</param>
        /// <param name="profileId">The ID of the profile that likes a certain case.</param>
        /// <param name="amount">The amount of cases that needs to get returned after a like.</param>
        /// <param name="minimalScore">The minimal score that a case should have. 0 will show all cases.</param>
        /// <response code ="200">Ok When the like has succesfully been stored in the database.</response>
        /// <response code ="403">Forbid When the logged in user isn't the same as the user who wants to give the like.</response>
        /// <response code ="404">NotFound when the profile isn't found.</response>
        /// <response code ="400">BadRequest when the like isn't put into the datasource.</response>
        [HttpPost("{caseId}/Like/{profileId}/{amount}/{minimalScore}")]
        [Authorize]
        public IActionResult Like(int caseId, int profileId, int amount, int minimalScore)
        {
            Profile profile = _profileRepo.GetById(profileId);
            Case foundCase = _cacheCases.FirstOrDefault(c => c.Id.Equals(caseId));

            if (profile != null && foundCase != null)
            {
                    Appreciation appreciation = _logic.Like(_caseRepo, _attributeRepo, _appreciationRepo, _profileRepo.GetById(profileId), caseId, _cacheAttributes, _cacheCases);
                    if (appreciation != null)
                    {
                        _logger.InsertLog(DbLogger.LoggingEvents.INFORMATION.ToString(), $"Successfully liked a case at CasesController.Like() value with attribute type T where caseId is {caseId} and profileId is {profileId}.");
                        return Ok(_logic.GetSortedMatches(_profileRepo, _attributeRepo, _caseRepo, profile, _cacheAttributes, _memoryCache).FindAll(x => x.MatchScore >= minimalScore).Take(amount));
                    }
                    _logger.InsertLog(DbLogger.LoggingEvents.WARNING.ToString(), $"Couldn't post a new like at CasesController.Like() with caseId being {caseId} and profileId being {profileId}. This could mean that an error occurred.");
                    return BadRequest();
                }
            _logger.InsertLog(DbLogger.LoggingEvents.WARNING.ToString(), $" Couldn't post a new like at CasesController.Like() because the profile or profile authId is zero. This could mean that a wrong JSON was passed, no profile with this id being {profileId} could be found or an error occurred. ");
            return NotFound();
        }

        /// <summary>
        /// Method to give a dislike to a case by a profile.
        /// This can only be used when the user is logged in and the correct bearer token is being set into the header
        /// and the logged in user is the same user that wants to give the dislike.
        /// </summary>
        /// <param name="caseId">The ID of the case that has to receive a dislike from the profile.</param>
        /// <param name="profileId">The ID of the profile that dislikes a certain case.</param>
        /// <param name="amount">Amount of cases that needs to be returned after the dislike.</param>
        /// <param name="minimalScore">The minimal score that a case should have. 0 will give all cases.</param>
        /// <response code ="200">Ok When the dislike has succesfully been stored in the database.</response>
        /// <response code ="403">Forbid When the logged in user isn't the same as the user who wants to give the dislike.</response>
        /// <response code ="400">BadRequest when the dislike isn't put into the datasource.</response>
        /// <response code ="404">NotFound when the profile isn't found.</response>
        [HttpPost("{caseID}/Dislike/{profileID}/{amount}/{minimalScore}")]
        [Authorize]
        public IActionResult Dislike(int caseId, int profileId, int amount, int minimalScore)
        {
            string token = HttpContext.Request.Cookies["UserToken"];
            string authIdString = HttpContext.Request.Cookies[token + " authId"];
            Profile profile = _profileRepo.GetById(profileId);
            Case foundCase = _cacheCases.FirstOrDefault(c => c.Id.Equals(caseId));

            if (profile != null && foundCase != null)
            {
                    Appreciation appreciation = _logic.Dislike(_caseRepo, _attributeRepo, _appreciationRepo, _profileRepo.GetById(profileId), caseId, _cacheAttributes, _cacheCases);
                    if (appreciation != null)
                    {
                        _logger.InsertLog(DbLogger.LoggingEvents.INFORMATION.ToString(), $"Successfully posted a new dislike at CasesController.Dislike() with caseId being {caseId} and profileId being {profileId}.");
                        return Ok(_logic.GetSortedMatches(_profileRepo, _attributeRepo, _caseRepo, profile, _cacheAttributes, _memoryCache).FindAll(x => x.MatchScore >= minimalScore).Take(amount));
                    }
                    _logger.InsertLog(DbLogger.LoggingEvents.WARNING.ToString(), $"Couldn't post a new dislike at CasesController.Dislike() because the appreciation is null with caseId being {caseId} and profileId being {profileId}. This could mean that an error occurred.");
                    return BadRequest();
            }
            _logger.InsertLog(DbLogger.LoggingEvents.WARNING.ToString(), $"Couldn't post a new dislike at CasesController.Dislike() because the profile is null with profileId being {profileId}. This could mean that there is no such profile in the datasource or that an error occurred with getting the profile from the datasource.");
            return NotFound();
        }
    }
}