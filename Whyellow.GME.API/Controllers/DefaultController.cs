﻿using Microsoft.AspNetCore.Mvc;

namespace Whyellow.GME.API.Controllers
{
    /// <summary>
    /// Class used for integration test.
    /// </summary>
    [Produces("application/json")]
    [Route("api/Default")]
    public class DefaultController : Controller
    {
        /// <summary>
        /// Method to test the response for integration tests.
        /// </summary>
        /// <returns>String used for integration testing.</returns>
        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpGet]
        public string Get()
        {
            return "integration test";
        }
    }
}