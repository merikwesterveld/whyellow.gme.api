﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Whyellow.GME.API.Data.PostgreSQL;
using Whyellow.GME.API.IRepositories;
using Whyellow.GME.API.Models;
using ILogger = Whyellow.GME.API.IRepositories.ILogger;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Whyellow.GME.API.Controllers
{
    /// <summary>
    /// Controller that handles the calls for the matches between a profile and the cases.
    /// </summary>
    [Route("api/[controller]")]
    // ReSharper disable once InheritdocConsiderUsage
    public class MatchesController : Controller
    {
        /// <summary>
        /// Data source attribute repository used by the matches controller.
        /// </summary>
        private readonly IAttributeRepo _attributeRepo;

        /// <summary>
        /// Data source case repository used by the matches controller.
        /// </summary>
        private readonly ICaseRepo _caseRepo;

        /// <summary>
        /// Data source profile repository used by the matches controller.
        /// </summary>
        private readonly IProfileRepo _profileRepo;

        /// <summary>
        /// Logic class to handle the logic for the match controlles.
        /// </summary>
        private readonly Logic _logic;

        /// <summary>
        /// Memory cache object to use the data.
        /// </summary>
        private readonly IMemoryCache _memoryCache;

        /// <summary>
        /// Logger used in the application to log the application.
        /// </summary>
        private readonly ILogger _logger;

        /// <summary>
        /// Attributes out the cache used by this controller.
        /// </summary>
        private readonly List<Attribute> _cacheAttributes;

        /// <summary>
        /// Constructor to build up the match controller.
        /// </summary>
        /// <param name="attributeRepo">The attribute repository that is being used by the matches controller.</param>
        /// <param name="caseRepo">The case repository that is being used by the matches controller.</param>
        /// <param name="profileRepo">The profile repository that is being used by the matches controller.</param>
        /// <param name="memoryCache">The used memory cache object.</param>
        /// <param name="logger"></param>
        public MatchesController(IAttributeRepo attributeRepo, ICaseRepo caseRepo, IProfileRepo profileRepo, IMemoryCache memoryCache, ILogger logger)
        {
            _logger = logger;
            _attributeRepo = attributeRepo;
            _caseRepo = caseRepo;
            _profileRepo = profileRepo;
            _logic = new Logic();
            _memoryCache = memoryCache;
            _cacheAttributes = new List<Attribute>();

            if (!memoryCache.TryGetValue("Attributes", out _cacheAttributes) && _attributeRepo != null)
            {
                _cacheAttributes = _attributeRepo.GetAll();
                _memoryCache.Set("Attributes", _cacheAttributes);
            }
        }

        /// <summary>
        /// Method to get all the matches between the user and all the cases in order of match score. Only the matches 
        /// that the user didn't already like or dislike are shown.
        /// This can only be used when the user is logged in and the correct bearer token is being set into the header
        /// and the logged in user is the same user that wants to give the dislike.
        /// </summary>
        /// <returns>A list of matches sorted on their match score.</returns>
        /// <response code ="200">Ok when the user is logged in, the attributes are filled in correct and the matches can be shown.</response>
        /// <response code ="404">NotFound when the profile with the id can't be found.</response>
        /// <response code ="403">Forbid when the logged in user isn't the user that needs to get the matches.</response>
        /// <response code ="400">BadRequest when the input of the attributes of the profile isnt't correct.</response>
        [HttpGet("{profileId}/{amount}/{minimalScore}")]
        [Produces("application/json", Type = typeof(List<Match>))]
        [Authorize]
        public IActionResult Get(int profileId, int amount, int minimalScore)
        {
                Profile profile = _profileRepo.GetById(profileId);

                if (profile == null)
                {
                    _logger.InsertLog(DbLogger.LoggingEvents.WARNING.ToString(),
                        $"Couldn't get the matches at MatchesController.Get() for profile with profileId being {profileId}. This could happen because the profile couldn\'t be found in the datsource or an error occurred while searching for that specific profile.");
                    return NotFound();
                }
                    if (!profile.CheckProfileInput(profile, _cacheAttributes))
                    {
                        _logger.InsertLog(DbLogger.LoggingEvents.WARNING.ToString(),
                            $"Couldn\'t get the matches at MatchesController.Get() for the profile wih profileId being {profileId}. This could be because the posted attributes of the profile aren\'t correct or an error occurred.");
                        return BadRequest();
                    }
                    _logger.InsertLog(DbLogger.LoggingEvents.INFORMATION.ToString(),
                        $"Successfully got all the matches at MatchesController.Get() for profileId {profileId}.");
                    return Ok(_logic.GetSortedMatches(_profileRepo, _attributeRepo, _caseRepo, profile,
                        _cacheAttributes, _memoryCache).FindAll(x => x.MatchScore >= minimalScore).Take(amount));
        }


        /// <summary>
        /// Method to get the best case for the given profile based on the match score. 
        /// This can only be used when the user is logged in and the correct bearer token is being set into the header
        /// and the logged in user is the same user that wants to give the dislike.
        /// </summary>
        /// <param name="profileId">The id of the profile.</param>
        /// <returns>The best case for the profile based on the matchscore.</returns>
        /// <response code ="200">Ok when the user is logged in, the attributes are filled in correct and the best match case 
        /// can be shown.</response>
        /// <response code ="404">NotFound when the profile with the id can't be found.</response>
        /// <response code ="403">Forbid when the logged in user isn't the user that needs to get the best match.</response>
        /// <response code ="400">BadRequest when the input of the attributes of the profile isnt't correct.</response>
        [HttpGet("best/{profileId}")]
        [Produces("application/json", Type = typeof(Case))]
        [Authorize]
        public IActionResult BestCase(int profileId)
        {
            Profile profile = _profileRepo.GetById(profileId);
            string token = HttpContext.Request.Cookies["UserToken"];
            string authIdString = HttpContext.Request.Cookies[token + " authId"];

            if (profile == null)
            {
                _logger.InsertLog(DbLogger.LoggingEvents.WARNING.ToString(), $"Couldn't get the best match at MatchesController.BestCase() for profile with profileId being {profileId}. This could happen because the profile couldn\'t be found in the datsource or an error occurred while searching for that specific profile.");
                return NotFound();
            }

            if (profile.authId.Equals(authIdString))
            {
                if (!profile.CheckProfileInput(profile, _cacheAttributes))
                {
                    _logger.InsertLog(DbLogger.LoggingEvents.WARNING.ToString(), $"Couldn\'t get the best match at MatchesController.BestCase() for the profile wih profileId being {profileId}. This could be because the posted attributes of the profile aren't correct or an error occurred.");
                    return BadRequest();
                }

                List<Match> allMatches = _logic.GetSortedMatches(_profileRepo, _attributeRepo, _caseRepo, profile, _cacheAttributes, _memoryCache);

                if (allMatches != null)
                {
                    _logger.InsertLog(DbLogger.LoggingEvents.INFORMATION.ToString(), $"Successfully got all the best match at MatchesController.BestCase() for profileId {profileId}.");
                    return Ok(allMatches[0].MatchCase);
                }
            }
            _logger.InsertLog(DbLogger.LoggingEvents.INFORMATION.ToString(), $"Couldn\'t get the best match at MatchesController.BestCase() for the profile with profileId being {profileId}. This because the AuthId of the profile isn't the same as the logged in user.");
            return Forbid();
        }
    }
}