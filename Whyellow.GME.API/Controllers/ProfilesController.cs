﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Auth0.AuthenticationApi;
using Auth0.AuthenticationApi.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Whyellow.GME.API.Data.PostgreSQL;
using Whyellow.GME.API.IRepositories;
using Whyellow.GME.API.Models;
using Attribute = Whyellow.GME.API.Models.Attribute;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Whyellow.GME.API.Controllers
{
    /// <summary>
    /// Controller that handles all the calls to the profiles.
    /// </summary>
    [Route("api/[controller]")]
    // ReSharper disable once InheritdocConsiderUsage
    public class ProfilesController : Controller
    {
        /// <summary>
        /// Repository for the data source that is being used by this controller.
        /// </summary>
        private readonly IProfileRepo _profileRepo;

        /// <summary>
        /// Configuration used by the profiles controller.
        /// </summary>
        private readonly IConfiguration _configuration;

        /// <summary>
        /// Logger used for logging the application.
        /// </summary>
        private readonly ILogger _logger;

        private readonly Logic _logic;

        private readonly IAttributeRepo _attributeRepo;

        private readonly ICaseRepo _caseRepo;

        private readonly IMemoryCache _memoryCache;

        /// <summary>
        /// List of attributes from the cache.
        /// </summary>
        private readonly List<Attribute> _cacheAttributes;

        /// <summary>
        /// Constructor to instansiate the data source repository/
        /// </summary>
        /// <param name="profileRepo">The used repository to get all the data.</param>
        /// <param name="caseRepo">The used case repository.</param>
        /// <param name="attributeRepo">The used attribute repository.</param>
        /// <param name="configuration">The configuration used.</param>
        /// <param name="logger">Logger used to log all the events in the application.</param>
        /// <param name="memoryCache">Memory Cache used for getting the cache.</param>
        public ProfilesController(IProfileRepo profileRepo, ICaseRepo caseRepo, IAttributeRepo attributeRepo, IConfiguration configuration, ILogger logger, IMemoryCache memoryCache)
        {
            _profileRepo = profileRepo;
            _configuration = configuration;
            _logger = logger;
            _cacheAttributes = new List<Attribute>();
            _logic = new Logic();
            _attributeRepo = attributeRepo;
            _caseRepo = caseRepo;
            _memoryCache = memoryCache;

            if (!_memoryCache.TryGetValue("Attributes", out _cacheAttributes) && attributeRepo != null)
            {
                _cacheAttributes = attributeRepo.GetAll();
                _memoryCache.Set("Attributes", _cacheAttributes);
            }
        }


        /// <summary>
        /// Method to get all the profiles from the datasource.
        /// This can only be used when the user is logged in and the correct bearer token is being set into the header
        /// </summary>
        /// <returns>A list of profiles that are available in the datasource.</returns>
        /// <response code ="200">Ok when there are 1 or more available profiles in the datasource.</response>
        /// <response code ="404">NotFound when 0 profiles are available in the datasource.</response>
        [HttpGet]
        [Produces("application/json", Type = typeof(List<Profile>))]
        [Authorize]
        public IActionResult Get()
        {
            List<Profile> profiles = _profileRepo.GetAll();
            if (profiles != null)
            {
                _logger.InsertLog(DbLogger.LoggingEvents.INFORMATION.ToString(), "Successfully got all the profiles at ProfilesController.Get().");
                return Ok(profiles);
            }
            _logger.InsertLog(DbLogger.LoggingEvents.WARNING.ToString(), "Couldn't get all the profiles at ProfilesController.Get(). This could mean that no profiles are present in the datasource or an error occurred.");
            return NotFound();
        }

        /// <summary>
        /// Method to login with the credentials of a user (email and password).
        /// </summary>
        /// <param name="loginModel">The model for logging in to the api.</param>
        /// <returns>The acces token which should be placed into the header.</returns>
        /// <response code ="200">Ok when the user is succesfuly being logged into the system.</response>
        /// <response code ="400">Badrequest when the user can't log in.</response>
        [HttpPost("login")]
        public async Task<IActionResult> Login([FromBody] LoginModel loginModel)
        {
            try
            {
                AuthenticationApiClient client =
                    new AuthenticationApiClient(new Uri($"https://{_configuration["Auth0:Domain"]}/"));

                var result = await client.GetTokenAsync(new ResourceOwnerTokenRequest
                {
                    ClientId = loginModel.client_id,
                    ClientSecret = "9aUj28KznbHnxGigbTcscLLFZxnKpnaZLZIYsAGF9_6rSGUH3BWUBfjVLbel8AN0",
                    Scope = "openid profile",
                    Realm = loginModel.connection,
                    Username = loginModel.email,
                    Password = loginModel.password
                });

                var user = await client.GetUserInfoAsync(result.AccessToken);

                if (user != null)
                {
                    var claimsPrincipal = new ClaimsPrincipal(new ClaimsIdentity(new[]
                    {
                        new Claim(ClaimTypes.NameIdentifier, user.UserId),
                        new Claim(ClaimTypes.Name, user.NickName)
                    }, CookieAuthenticationDefaults.AuthenticationScheme));

                    await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, claimsPrincipal);

                    string accessToken = await GetAccessToken();

                    _logger.InsertLog(DbLogger.LoggingEvents.INFORMATION.ToString(),
                        "User successfully logged in at ProfilesController.Login().");
                    Token token = new Token(accessToken, _profileRepo.GetByAuthId(user.UserId));
                    return Ok(token);
                }
                return BadRequest();
            }
            catch (Exception e)
            {
                _logger.InsertLog(DbLogger.LoggingEvents.ERROR.ToString(), $"User couldn\'t get logged in at ProfilesController.Login(). This could mean that the combination of the email and password is unknown or an other error occurred. Stacktrace: {e.Message}.");
                return BadRequest();
            }
        }

        /// <summary>
        /// Method to log the user out of the system. This will remove all the cookies.
        /// </summary>
        /// <returns>.</returns>
        /// <response code ="200">Always ok.</response>
        [HttpPost("logout")]
        public IActionResult Logout()
        {
            string token = HttpContext.Request.Cookies["UserToken"];
            if (!String.IsNullOrEmpty(token))
            {
                HttpContext.Response.Cookies.Delete("UserToken");
            }

            if (HttpContext.Request.Cookies[token + " authId"] != null)
            {
                HttpContext.Response.Cookies.Delete(token + " authId");
            }

            _logger.InsertLog(DbLogger.LoggingEvents.INFORMATION.ToString(), "A user has successfully logged out at ProfilesController.Logout().");
            return Ok();
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task<string> GetAccessToken()
        {
            HttpClient httpClient = new HttpClient();
            Uri uri = new Uri("https://whyellow-gme-api.eu.auth0.com/oauth/token");
            string json =
                "{\"grant_type\":\"client_credentials\",\"client_id\": \"K9lN4brozslLrUEs0EeNIkLrgiCgV2WU\",\"client_secret\": \"9aUj28KznbHnxGigbTcscLLFZxnKpnaZLZIYsAGF9_6rSGUH3BWUBfjVLbel8AN0\",\"audience\": \"https://whyellow-gme-api.eu.auth0.com/api/v2/\"}";
            var response = await httpClient.PostAsync(uri,
                new StringContent(json, Encoding.UTF8, "application/json"));
            var resulttoken = await response.Content.ReadAsStringAsync();
            return JObject.Parse(resulttoken)["access_token"].ToString();
        }

        /// <summary>
        /// Method to register a new user into the API.
        /// </summary>
        /// <param name="loginModel">The log in model that needs to get registered.</param>
        /// <returns>The response of Auth0.</returns>
        /// <response code ="200">Ok when the user is succesfully registered.</response>
        /// <response code ="400">BadRequest when the user can't get registered.</response>
        [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody] LoginModel loginModel)
        {
            if (loginModel != null)
            {
                try
                {
                    HttpClient client = new HttpClient();
                    Uri uri = new Uri("https://whyellow-gme-api.eu.auth0.com/dbconnections/signup");
                    string json = JsonConvert.SerializeObject(loginModel);
                    var response =
                        await client.PostAsync(uri, new StringContent(json, Encoding.UTF8, "application/json"));

                    if (response.IsSuccessStatusCode)
                    {
                        Tuple<UserInfo, string> tuple = await LoginUser(loginModel);
                        UserInfo user = tuple.Item1;
                        string accessToken = tuple.Item2;

                        Profile profile = new Profile(-1)
                        {
                            authId = user.UserId
                        };
                        if (profile.CheckProfileInput(profile, _cacheAttributes))
                        {
                            Profile profileInserted = _profileRepo.Insert(profile);
                            if (profileInserted != null)
                            {
                                _logger.InsertLog(DbLogger.LoggingEvents.INFORMATION.ToString(),
                                    $"Successfully registered a new user with email being {loginModel.email} at ProfilesController.Register().");
                                Token token = new Token(accessToken, _profileRepo.GetByAuthId(profileInserted.authId));
                                return Ok(token);
                            }
                        }
                        _logger.InsertLog(DbLogger.LoggingEvents.WARNING.ToString(),
                            $"Couldn't register the new user with email {loginModel.email} at ProfilesController.Register(). This could mean that the new user has some invalid attribute values or an error occured.");
                    }
                    _logger.InsertLog(DbLogger.LoggingEvents.ERROR.ToString(),
                        $"Couldn't register the new user with the email {loginModel.email} at ProfilesController.Register(). This could mean that the user email is already registered. Respons: ${response.Content}");
                    return BadRequest();
                }
                catch (Exception e)
                {
                    _logger.InsertLog(DbLogger.LoggingEvents.ERROR.ToString(),
                        $"Couldn't register the new user with the email {loginModel.email} at ProfilesController.Register(). This could mean that the user email is already registered or an other error occurred with Auth0, location: {e.Source} stacktrace: {e.Message} & {e.TargetSite} & {e.StackTrace}");
                    return BadRequest();
                }
            }
            return BadRequest();
        }

        /// <summary>
        /// Method to log the user into the api (for registering).
        /// </summary>
        /// <param name="value">The login model.</param>
        /// <returns>The logged in user.</returns>
        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task<Tuple<UserInfo, string>> LoginUser(LoginModel value)
        {
            if (ModelState.IsValid)
            {
                AuthenticationApiClient client =
                    new AuthenticationApiClient(new Uri($"https://{_configuration["Auth0:Domain"]}/"));

                var result = await client.GetTokenAsync(new ResourceOwnerTokenRequest
                {
                    ClientId = value.client_id,
                    ClientSecret = "9aUj28KznbHnxGigbTcscLLFZxnKpnaZLZIYsAGF9_6rSGUH3BWUBfjVLbel8AN0",
                    Scope = "openid profile",
                    Realm = value.connection,
                    Username = value.email,
                    Password = value.password
                });

                var user = await client.GetUserInfoAsync(result.AccessToken);

                var claimsPrincipal = new ClaimsPrincipal(new ClaimsIdentity(new[] {
                    new Claim(ClaimTypes.NameIdentifier, user.UserId),
                    new Claim(ClaimTypes.Name, user.NickName)
                }, CookieAuthenticationDefaults.AuthenticationScheme));

                await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, claimsPrincipal);

                string accesstoken = await GetAccessToken();

                return Tuple.Create(user, accesstoken);
            }
            return null;
        }

        /// <summary>
        /// Method to get the best case for the given profile based on the match score. 
        /// This can only be used when the user is logged in and the correct bearer token is being set into the header
        /// and the logged in user is the same user that needs to get updated.
        /// </summary>
        /// <param name="profile">The profile that needs to be updated in the datasource.</param>
        /// <param name="percentageAmount">The amount of cases that are returned with an match score of at least the percentage amount.</param>
        /// <response code ="200">Ok when the user is succesfuly updated.</response>
        /// <response code ="400">BadRequest when the given user isn't correct.</response>
        /// <response code ="404">NotFound when the profile can't be found.</response>
        /// <response code ="403">Forbid when the logged in user isn't the same as the user that needs to get
        /// updated.</response>
        [HttpPut("{percentageAmount}")]
        [Authorize]
        public IActionResult Put([FromBody] Profile profile, int percentageAmount)
        {
            if (profile == null)
            {
                _logger.InsertLog(DbLogger.LoggingEvents.WARNING.ToString(), $"Couldn\'t update the profile at ProfilesController.Put() because the profile is null or the profile couldn't be found in the datasource. This could mean an error occurred or no such profile is present in the datasource.");
                return BadRequest();
            }

                if (profile.CheckProfileInput(profile, _cacheAttributes))
                {
                    if (profile.Attributes.Count > 0)
                    {
                        Profile profileUpdated = _profileRepo.Update(profile);
                        if (profileUpdated != null)
                        {
                            _logger.InsertLog(DbLogger.LoggingEvents.INFORMATION.ToString(),
                                $"Succesfully updated the profile with profileId {profile.Id}. at ProfilesController.Put()");
                        var amount = _logic.GetSortedMatches(_profileRepo, _attributeRepo, _caseRepo, profileUpdated,
                                _cacheAttributes,
                                _memoryCache).FindAll(x => x.MatchScore > percentageAmount).Count;
                            return Ok(amount);
                        }
                        _logger.InsertLog(DbLogger.LoggingEvents.WARNING.ToString(), $"Couldn't update the profile at ProfilesController.Put() with profileId {profile.Id} because an error occurred.");
                        BadRequest();
                    }
                    else
                    {
                        var amount = _logic.GetSortedMatches(_profileRepo, _attributeRepo, _caseRepo, profile,
                            _cacheAttributes,
                            _memoryCache).FindAll(x => x.MatchScore >= percentageAmount).Count;
                        return Ok(amount);
                    }
                }
                else
                {
                    _logger.InsertLog(DbLogger.LoggingEvents.WARNING.ToString(), $"Couldn't update the profile at ProfilesController.Put() with profileId {profile.Id} because the attributes given by the profile were incorrect.");
                    return BadRequest();
                }
                return BadRequest();
        }
    }
}