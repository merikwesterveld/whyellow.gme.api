﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Whyellow.GME.API.Data.PostgreSQL;
using Whyellow.GME.API.IRepositories;
using Attribute = Whyellow.GME.API.Models.Attribute;
using ILogger = Whyellow.GME.API.IRepositories.ILogger;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Whyellow.GME.API.Controllers
{
    /// <summary>
    /// Controller that controlls all the actions for the attribute.
    /// </summary>
    [Route("api/[controller]")]
    public class AttributesController : Controller
    {
        /// <summary>
        /// Logger used in the application for application logging.
        /// </summary>
        private readonly ILogger _logger;

        /// <summary>
        /// List of attributes from the cache. 
        /// </summary>
        private readonly List<Attribute> _cacheAttributes;

        /// <summary>
        /// Constructor to use the data repository.
        /// </summary>
        /// <param name="attributeRepo">The used data repository.</param>
        /// <param name="memoryCache">The memory cache.</param>
        /// <param name="logger">Logger for this controller to log all the activity.</param>
        public AttributesController(IAttributeRepo attributeRepo, IMemoryCache memoryCache, ILogger logger)
        {
            _logger = logger;
            _cacheAttributes = new List<Attribute>();

            if (!memoryCache.TryGetValue("Attributes", out _cacheAttributes) && attributeRepo != null)
            {
                _cacheAttributes = attributeRepo.GetAll();
                memoryCache.Set("Attributes", _cacheAttributes);
            }
        }

        /// <summary>
        /// Method to get all the available attributes from the data source which the profile needs to fill in. 
        /// This can only be used when the user is logged in and the correct bearer token is being set into the header.
        /// </summary>
        /// <returns>A list of all the available attributes that the profile needs to fill in.</returns>
        /// <response code ="200">Ok when 1 or more attributes are available.</response>
        /// <response code ="404">NotFound when 0 attributes are available.</response>
        [Produces("application/json", Type = typeof(List<Attribute>))]
        [HttpGet]
        [Authorize]
        public IActionResult Get()
        {
            if (_cacheAttributes != null)
            {
                _logger.InsertLog(DbLogger.LoggingEvents.INFORMATION.ToString(), "Got attributes successfully at AttributesController.Get().");
                return Ok(_cacheAttributes.Where(a => !a.Type.Abbreviation.Equals("T")).ToList());
            }
            _logger.InsertLog(DbLogger.LoggingEvents.WARNING.ToString(), "Couldn't get attributes at AttributesController.Get(). There are either no attributes in the datasource or an error occurred in getting attributes from the datasource.");
            return NotFound();
        }

        /// <summary>
        /// Method to get an attribute by name. 
        /// This can only be used when the user is logged in and the correct bearer token is being set into the header.
        /// </summary>
        /// <returns>A list of all the available attributes.</returns>
        /// <response code ="200">Ok when the attribute with the name has been found.</response>
        /// <response code ="404">NotFound when the attribute searched for can't be found.</response>
        [HttpGet("{attributeName}")]
        [Produces("application/json", Type = typeof(Attribute))]
        [Authorize]
        public IActionResult GetByName(string attributeName)
        {
            Attribute attribute = _cacheAttributes.FirstOrDefault(a => a.Name.ToLowerInvariant().Equals(attributeName.ToLowerInvariant()));

            if (attribute != null)
            {
                _logger.InsertLog(DbLogger.LoggingEvents.INFORMATION.ToString(), $"Got an attribute by name succesfully at AttributesController.Get() with name being {attributeName}.");
                return Ok(attribute);
            }
            _logger.InsertLog(DbLogger.LoggingEvents.WARNING.ToString(), $"Couldn't get the attribute with name being {attributeName}. This could mean that there is no attribute with that name or an error occurred with getting attributes from the datasource.");
            return NotFound();
        }
    }
}