﻿using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Whyellow.GME.API.Data.Excel;
using Whyellow.GME.API.Data.PostgreSQL;
using Whyellow.GME.API.IRepositories;
using Whyellow.GME.API.Models;

namespace Whyellow.GME.API.UnitTestApplication
{
    /// <summary>
    /// Test class that tests the attributetype repo's and logic.
    /// </summary>
    [TestClass]
    public class AttributeTypeTest
    {
        private readonly ExcelAttributeTypeRepo _excelAttributeTypeRepo;
        private readonly PostgreSQLAttributeTypeRepo _postgreSqlAttributeTypeRepo;

        DatabaseOptions options = new DatabaseOptions();

        /// <summary>
        /// Arrange constructor for the attribute type test.
        /// </summary>
        public AttributeTypeTest()
        {
            options.Configure("Host=bj9hnxfvkxfbwkt-postgresql.services.clever-cloud.com;Port=5432;Database=bj9hnxfvkxfbwkt;Username=uvuifnnyjdifabkvlf3s;Password=tlCXFNFYBMa3FlNTSYOQ;Maximum Pool Size=4");

            // Arrange
            _excelAttributeTypeRepo = new ExcelAttributeTypeRepo(new DbLogger(options));
            _postgreSqlAttributeTypeRepo = new PostgreSQLAttributeTypeRepo(new DbLogger(options), options);
        }

        /// <summary>
        /// Method to test the get all method from the attribute types repo.
        /// </summary>
        [TestMethod]
        public void TestAttributeTypeGetAll()
        {
            // Act
            List<AttributeType> AttributeTypesPostgres = _postgreSqlAttributeTypeRepo.GetAll();
            List<AttributeType> AttributeTypesExcel = _excelAttributeTypeRepo.GetAll();
            bool excel = false;
            bool postgres = false;

            if (AttributeTypesExcel != null)
            {
                excel = true;
            }
            if (AttributeTypesPostgres != null)
            {
                postgres = true;
            }

            // Assert
            Assert.AreEqual(excel, true);
            Assert.AreEqual(postgres, true);
        }

        /// <summary>
        /// Method to test the get by id method for the attributes type.
        /// </summary>
        [TestMethod]
        public void TestAttributeTypeGetByID()
        {
            // Act
            AttributeType postgresAttributeType = _postgreSqlAttributeTypeRepo.GetById(1);
            AttributeType excelAttributeType = _excelAttributeTypeRepo.GetById(1);

            // Assert
            Assert.AreEqual(postgresAttributeType.Id, 1);
            Assert.AreEqual(excelAttributeType.Id, 1);
        }

        /// <summary>
        /// Method to test the get by id method with a wrong or unknown id.
        /// </summary>
        [TestMethod]
        public void TestAttributeTypeGetByIDWrong()
        {
            // Act
            AttributeType postgresAttributeType = _postgreSqlAttributeTypeRepo.GetById(-1);
            AttributeType excelAttributeType = _excelAttributeTypeRepo.GetById(-1);

            // Assert
            Assert.AreEqual(postgresAttributeType, null);
            Assert.AreEqual(excelAttributeType, null);
        }
    }
}
