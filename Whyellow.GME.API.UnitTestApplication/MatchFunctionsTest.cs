﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Whyellow.GME.API.Models;

namespace Whyellow.GME.API.UnitTestApplication
{
    /// <summary>
    /// Test class to test all the match functions.
    /// </summary>
    [TestClass]
    public class MatchFunctionsTest
    {
        /// <summary>
        /// The matchfunction class to call all the matchfunctions.
        /// </summary>
        private readonly MatchFunction _matchFunction = new MatchFunction();

        /// <summary>
        /// Test method to test the No Range match function while it should be a 100% positive match. 
        /// </summary>
        [TestMethod]
        public void RNPositive()
        {
            //arrange
            double profileValue = 3;
            double caseValue = 3;

            //act
            double score = _matchFunction.RN(profileValue, caseValue);

            //assert
            Assert.AreEqual(100, score, "The score isn't correct, it should be 100 because the profile and case value are the same.");
        }

        /// <summary>
        /// Test metod to test the No Range match function while it should be a 0% match. 
        /// </summary>
        [TestMethod]
        public void RNNegative()
        {
            //arrange
            double profileValue = 3;
            double caseValue = 2;

            //act
            double score = _matchFunction.RN(profileValue, caseValue);

            //assert
            Assert.AreEqual(0, score, "The score isn't correct, it should be 0 because the profile and case value aren't the same.");
        }

        /// <summary>
        /// Test method to test the Fit Range match function while it should be a 100% positive match.
        /// </summary>
        [TestMethod]
        public void RFPositive()
        {
            //arrange
            double profileValue = 5;
            double caseBottom = 3;
            double caseTop = 6;

            //act
            double score = _matchFunction.RF(profileValue, caseBottom, caseTop);

            //assert
            Assert.AreEqual(100, score, "The score should be 100 because the profile value of 5 fits in the case range of 3 and 6.");
        }

        /// <summary>
        /// Test Method to test the Fit Range match function while it should be a 0% match.
        /// </summary>
        [TestMethod]
        public void RFNegative()
        {
            //arrange
            double profileValue = 1;
            double caseBottom = 3;
            double caseTop = 5;

            //act
            double score = _matchFunction.RF(profileValue, caseBottom, caseTop);

            //assert
            Assert.AreEqual(0, score, "The score should be 0 because the profile value of 1 doesn't fit in the case range of 3 and 5.");
        }

        /// <summary>
        /// Test method to test the Partial Range match function while it should be a 100% positive match on the top side.
        /// </summary>
        [TestMethod]
        public void RPPositiveTopSide()
        {
            //arrange
            double profileValue = 12;
            double caseValue = 10;
            double low = 7;
            double high = 13;
            double partial = 5;

            //act
            double score = _matchFunction.RP(low, high, partial, profileValue, caseValue);

            Assert.AreEqual(100, score, "The score should be 100 becasue the profile value fits in the top part of the ramp match function.");

        }

        /// <summary>
        /// Test method to test the Partial Range match function while it should be a 100% match on the bottom side.
        /// </summary>
        [TestMethod]
        public void RPPositiveBottomSide()
        {
            //arrange
            double profileValue = 8;
            double caseValue = 10;
            double low = 7;
            double high = 13;
            double partial = 5;

            //act
            double score = _matchFunction.RP(low, high, partial, profileValue, caseValue);

            Assert.AreEqual(100, score, "The score should be 100 becasue the profile value fits in the bottom part of the ramp match function.");
        }

        /// <summary>
        /// Test method to test the Partial Range match which should be partialy positive on the top side.
        /// </summary>
        [TestMethod]
        public void RPPositiveTopSidePartial()
        {
            //arrange
            double profileValue = 16;
            double caseValue = 10;
            double low = 1;
            double high = 3;
            double partial = 5;

            //act
            int score = (int) _matchFunction.RP(low, high, partial, profileValue, caseValue);

            //assert
            Assert.AreEqual(18, score, "The score should be 18 becasue the profile is on the partial bottom side.");
        }

        /// <summary>
        /// Test method to test the Partial Range match function which should be partialy positive on the bottom side. 
        /// </summary>
        [TestMethod]
        public void RPPositiveBottomSidePartial()
        {
            //arrange
            double profileValue = 6;
            double caseValue = 10;
            double low = 2;
            double high = 2;
            double partial = 5;

            //act
            int score = (int)_matchFunction.RP(low, high, partial, profileValue, caseValue);

            //assert
            Assert.AreEqual(60, score, "The score should be 60 becasue the profile is on the partial bottom side.");
        }

        /// <summary>
        /// Test method to test the Partial Range match function which should be negative bevause it is outside the range.
        /// </summary>
        [TestMethod]
        public void RPNegative()
        {
            //arrange
            double profileValue = 3;
            double caseValue = 12;
            double low = 2;
            double high = 2;
            double partial = 5;

            //act
            int score = (int)_matchFunction.RP(low, high, partial, profileValue, caseValue);

            //assert
            Assert.AreEqual(0, score, "The score should be 0 becasue the profile is outside the partial side.");
        }

        /// <summary>
        /// Test to test the Single Element match function which should be positive.
        /// </summary>
        [TestMethod]
        public void ESPositive()
        {
            //arrange 
            string profileValue = "red";
            string caseValue = "red";

            //act
            double score = _matchFunction.ES(profileValue, caseValue);

            //assert
            Assert.AreEqual(100, score, "The score should be 100 because the profile and case value are the same.");
        }

        /// <summary>
        /// Test method to test the Single Element match function which should be negative.
        /// </summary>
        [TestMethod]
        public void ESNegative()
        {
            //arrange 
            string profileValue = "red";
            string caseValue = "black";

            //act
            double score = _matchFunction.ES(profileValue, caseValue);

            //assert
            Assert.AreEqual(0, score, "The score should be 0 because the profile and case value aren't the same.");
        }

        /// <summary>
        /// Test method to test the Multiple Element match function which should be positive.
        /// </summary>
        [TestMethod]
        public void EMPositive()
        {
            //arrange 
            string profileValues = "red, black, blue";
            string caseValues = "black, red, blue";

            //act
            double score = _matchFunction.EM(profileValues, caseValues);

            //assert
            Assert.AreEqual(100, score, "The score should be 100 because the profile and case values intersect exactly.");
        }

        /// <summary>
        /// Test method to test the Multiple Element match function which should be partialy positive.
        /// </summary>
        [TestMethod]
        public void EMPartialyPositive()
        {
            //arrange 
            string profileValues = "red, black, blue";
            string caseValues = "black, purple, white, blue, yellow";

            //act
            int score = (int)_matchFunction.EM(profileValues, caseValues);

            //assert
            Assert.AreEqual(16, score, "The score should be 16 because the profile and case values intersect on a few basis.");
        }

        /// <summary>
        /// Test method to test the Multiple Element match function which should be negative.
        /// </summary>
        [TestMethod]
        public void EMNegative()
        {
            //arrange 
            string profileValues = "red, black, blue";
            string caseValues = "purple";

            //act
            int score = (int)_matchFunction.EM(profileValues, caseValues);

            //assert
            Assert.AreEqual(0, score, "The score should be 100 because the profile and case values intersect exactly.");
        }

        /// <summary>
        /// Test method to test the Type match function which should be above 50. 
        /// </summary>
        [TestMethod]
        public void TAbove50()
        {
            //arrange
            Profile p = new Profile(1);
            p.LikesAndDislikes.Add(new Appreciation(1, "type1", 4, Appreciation.TypeAppreciation.Like, 1));
            p.LikesAndDislikes.Add(new Appreciation(2, "type2", 8, Appreciation.TypeAppreciation.Like, 1));
            p.LikesAndDislikes.Add(new Appreciation(3, "type3", 1, Appreciation.TypeAppreciation.Like, 1));

            p.LikesAndDislikes.Add(new Appreciation(4, "type1", 0, Appreciation.TypeAppreciation.Dislike, 1));
            p.LikesAndDislikes.Add(new Appreciation(5, "type2", 2, Appreciation.TypeAppreciation.Dislike, 1));
            p.LikesAndDislikes.Add(new Appreciation(6, "type3", 5, Appreciation.TypeAppreciation.Dislike, 1));

            string caseValues = "type1,type2";

            //act
            int score = (int)_matchFunction.T(p, caseValues);

            //assert
            Assert.AreEqual(86, score, "The score should be 85 because of the like and dislike profile.");
        }

        /// <summary>
        /// Test method to test the Type match function which should be below 50.
        /// </summary>
        [TestMethod]
        public void TBelow50()
        {
            //arrange
            Profile p = new Profile(1);
            p.LikesAndDislikes.Add(new Appreciation(1, "type1", 2, Appreciation.TypeAppreciation.Like, 1));
            p.LikesAndDislikes.Add(new Appreciation(2, "type2", 10, Appreciation.TypeAppreciation.Like, 1));
            p.LikesAndDislikes.Add(new Appreciation(3, "type3", 3, Appreciation.TypeAppreciation.Like, 1));

            p.LikesAndDislikes.Add(new Appreciation(4, "type1", 13, Appreciation.TypeAppreciation.Dislike, 1));
            p.LikesAndDislikes.Add(new Appreciation(5, "type2", 1, Appreciation.TypeAppreciation.Dislike, 1));
            p.LikesAndDislikes.Add(new Appreciation(6, "type3", 5, Appreciation.TypeAppreciation.Dislike, 1));

            string caseValues = "type3,type1";

            //act
            int score = (int)_matchFunction.T(p, caseValues);

            //assert
            Assert.AreEqual(25, score, "The score should be 85 because of the like and dislike profile.");
        }
    }
}
