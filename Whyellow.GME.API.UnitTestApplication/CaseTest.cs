﻿using System.Collections.Generic;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Whyellow.GME.API.Data.Excel;
using Whyellow.GME.API.Data.PostgreSQL;
using Whyellow.GME.API.IRepositories;
using Whyellow.GME.API.Models;

namespace Whyellow.GME.API.UnitTestApplication
{
    /// <summary>
    /// Test method to test the case repo's and logic.
    /// </summary>
    [TestClass]
    public class CaseTest
    {
        private readonly ExcelCaseRepo _excelCaseRepo;
        private readonly PostgreSQLCaseRepo _postgreSqlCaseRepo;

        DatabaseOptions options = new DatabaseOptions();

        /// <summary>
        /// Arrange constructor for the test case.
        /// </summary>
        public CaseTest()
        {
            options.Configure("Host=bj9hnxfvkxfbwkt-postgresql.services.clever-cloud.com;Port=5432;Database=bj9hnxfvkxfbwkt;Username=uvuifnnyjdifabkvlf3s;Password=tlCXFNFYBMa3FlNTSYOQ;Maximum Pool Size=4");

            // Arrange
            _excelCaseRepo = new ExcelCaseRepo(new DbLogger(options), new MemoryCache(new MemoryCacheOptions()));
            _postgreSqlCaseRepo = new PostgreSQLCaseRepo(new DbLogger(options), new MemoryCache(new MemoryCacheOptions()), options);
        }

        /// <summary>
        /// Method to test the get all method for the cases.
        /// </summary>
        [TestMethod]
        public void TestCaseGetAll()
        {
            // Act
            List<Case> CasesPostgres = _postgreSqlCaseRepo.GetAll();
            List<Case> CasesExcel = _excelCaseRepo.GetAll();
            bool excel = false;
            bool postgres = false;

            if (CasesExcel != null)
            {
                excel = true;
            }
            if (CasesPostgres != null)
            {
                postgres = true;
            }

            // Assert
            Assert.AreEqual(excel, true);
            Assert.AreEqual(postgres, true);
        }

        /// <summary>
        /// Method to test the get by id method for the case.
        /// </summary>
        [TestMethod]
        public void TestCaseGetByID()
        {
            // Act
            Case postgresCase = _postgreSqlCaseRepo.GetById(1);
            Case excelCase = _excelCaseRepo.GetById(1);

            // Assert
            Assert.AreEqual(postgresCase.Id, 1);
            Assert.AreEqual(excelCase.Id, 1);
        }

        /// <summary>
        /// Method to test the get by id method for the case with a wrong / unknown id.
        /// </summary>
        [TestMethod]
        public void TestCaseGetByIDWrong()
        {
            // Act
            Case postgresCase = _postgreSqlCaseRepo.GetById(-1);
            Case excelCase = _excelCaseRepo.GetById(-1);

            // Assert
            Assert.AreEqual(postgresCase, null);
            Assert.AreEqual(excelCase, null);
        }
    }
}
