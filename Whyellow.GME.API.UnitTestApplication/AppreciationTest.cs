﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using Npgsql;
using Whyellow.GME.API.Data.Excel;
using Whyellow.GME.API.Data.PostgreSQL;
using Whyellow.GME.API.Models;

namespace Whyellow.GME.API.UnitTestApplication
{
    /// <summary>
    /// Test class to test the appreciation logic / repo's.
    /// </summary>
    [TestClass]
    public class AppreciationTest
    {
        private readonly PostgreSQLAppreciationRepo _postgreSqlAppreciationRepo;
        private readonly ExcelAppreciationRepo _excelAppreciationRepo;

        DatabaseOptions options = new DatabaseOptions();

        /// <summary>
        /// Arrange constructor.
        /// </summary>
        public AppreciationTest()
        {
            NpgsqlConnection.MapEnumGlobally<Appreciation.TypeAppreciation>();

            options.Configure("Host=bj9hnxfvkxfbwkt-postgresql.services.clever-cloud.com;Port=5432;Database=bj9hnxfvkxfbwkt;Username=uvuifnnyjdifabkvlf3s;Password=tlCXFNFYBMa3FlNTSYOQ;Maximum Pool Size=4");
            // Arrange
            _postgreSqlAppreciationRepo = new PostgreSQLAppreciationRepo(new DbLogger(options), options);
            _excelAppreciationRepo = new ExcelAppreciationRepo(new DbLogger(options));
        }

        /// <summary>
        /// Method to test the get all method for the appreciations.
        /// </summary>
        [TestMethod]
        public void TestAppreciationGetAll()
        {
            // Act
            List<Appreciation> appreciationsPostgres = _postgreSqlAppreciationRepo.GetAll();
            List<Appreciation> appreciationesExcel = _excelAppreciationRepo.GetAll();
            bool excel = false;
            bool postgres = false;

            if (appreciationesExcel != null)
            {
                excel = true;
            }
            if (appreciationsPostgres != null)
            {
                postgres = true;
            }

            // Assert
            Assert.AreEqual(excel, true);
            Assert.AreEqual(postgres, true);
        }

        /// <summary>
        /// Method to test the get by id method if a wrong / unknown ID is passed into the method.
        /// </summary>
        [TestMethod]
        public void TestAppreciationGetByIDWrong()
        {
            // Act
            Appreciation postgresAppreciation = _postgreSqlAppreciationRepo.GetById(-1);
            Appreciation excelAppreciation = _excelAppreciationRepo.GetById(-1);

            // Assert
            Assert.AreEqual(postgresAppreciation, null);
            Assert.AreEqual(excelAppreciation, null);
        }
    }
}
