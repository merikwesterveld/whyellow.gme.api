﻿using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Whyellow.GME.API.Data.Excel;
using Whyellow.GME.API.Data.PostgreSQL;
using Whyellow.GME.API.IRepositories;
using Whyellow.GME.API.Models;

namespace Whyellow.GME.API.UnitTestApplication
{
    /// <summary>
    /// Test class to test the attribute logic / repo's.
    /// </summary>
    [TestClass]
    public class AttributeTest
    {
        private readonly ExcelAttributeRepo _excelAttributeRepo;
        private readonly PostgreSQLAttributeRepo _postgreSqlAttributeRepo;

        DatabaseOptions options = new DatabaseOptions();

        /// <summary>
        /// Arrange constructor for the test class.
        /// </summary>
        public AttributeTest()
        {
            options.Configure("Host=bj9hnxfvkxfbwkt-postgresql.services.clever-cloud.com;Port=5432;Database=bj9hnxfvkxfbwkt;Username=uvuifnnyjdifabkvlf3s;Password=tlCXFNFYBMa3FlNTSYOQ;Maximum Pool Size=4");

            // Arrange
            _excelAttributeRepo = new ExcelAttributeRepo(new DbLogger(options));
            _postgreSqlAttributeRepo = new PostgreSQLAttributeRepo(new DbLogger(options), options);
        }

        /// <summary>
        /// Method to test the get all method for the attributes.
        /// </summary>
        [TestMethod]
        public void TestAttributeGetAll()
        {
            // Act
            List<Attribute> AttributesPostgres = _postgreSqlAttributeRepo.GetAll();
            List<Attribute> AttributesExcel = _excelAttributeRepo.GetAll();
            bool excel = false;
            bool postgres = false;

            if (AttributesExcel != null)
            {
                excel = true;
            }
            if (AttributesPostgres != null)
            {
                postgres = true;
            }

            // Assert
            Assert.AreEqual(excel, true);
            Assert.AreEqual(postgres, true);
        }

        /// <summary>
        /// Method to test the get id method for the attributes with a wrong or unknown id.
        /// </summary>
        [TestMethod]
        public void TestAttributeGetByIDWrong()
        {
            // Act
            Attribute postgresAttribute = _postgreSqlAttributeRepo.GetByName("WRONG123");
            Attribute excelAttribute = _excelAttributeRepo.GetByName("WRONG123");

            // Assert
            Assert.AreEqual(postgresAttribute, null);
            Assert.AreEqual(excelAttribute, null);
        }
    }
}
